const functions = require('firebase-functions');
const accountSid = 'AC574d3ee3b1626061246da3ac267cc8d6';
const authToken = '4c8e67aa4f051fd65d8871116ad24300';
const client = require('twilio')(accountSid, authToken);
const cors = require('cors')({origin: true});

exports.call = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        client.calls
        .create({
            url: 'http://demo.twilio.com/docs/voice.xml',
            to: request.query.to || '+18432091285',
            from: request.query.from || '+14159410905'
        })
        .then(call => response.send(call)
        ,err => response.send(err))
        .done();
    });
});
