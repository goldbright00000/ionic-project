(function() {
  'use strict';



  angular
    .module('purplePanda', [
      // Ionic modules.
      'ionic',
      'ionic.cloud',
      'ngCordova',

      // Third party modules.
      'firebase',
      'angularMoment',
      'ionic-numberpicker',
      'stripe.checkout',
      'star-rating', // :-)

      // Custom modules.
      'app.auth',
      'app.consumer',
      'app.core',
      'app.landing',
      'app.layout',
      'app.panda',
      'app.sign-in'
    ])
    .config(configFunction)
    .run(runFunction);

  configFunction.$inject = ['$urlRouterProvider', '$ionicCloudProvider'];

  function configFunction($urlRouterProvider, $ionicCloudProvider ) {

    // var STRIPE_API_PUBLISHABLE_KEY = "pk_test_0ioW37xsaCio77yLyNS1GEgl"
    // var STRIPE_API_PUBLISHABLE_KEY = "pk_live_KowOVeaKH9nz1drjfGB0pk0f"
    let TEST_STRIPE_API_PUBLISHABLE_KEY = 'pk_test_j4nJLAdxhucNlggWCKFSpEVs';
    let LIVE_STRIPE_API_PUBLISHABLE_KEY = "pk_live_gLPRTnMRkbMIaHb1eujG4epH"

    // stripeKey = ( CacheService.getMode() === 'qa' )
    //     ? TEST_STRIPE_API_PUBLISHABLE_KEY : LIVE_STRIPE_API_PUBLISHABLE_KEY;

    // StripeCheckoutProvider.defaults({
    //   key: LIVE_STRIPE_API_PUBLISHABLE_KEY
    // });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/landing');

    $ionicCloudProvider.init({
      'core': {
        'app_id': 'fe4e923a'
      },
      'push': {
        'sender_id': '781269879113',
        'pluginConfig': {
          'ios': {
            'badge': true,
            'sound': true
          },
          'android': {
            'iconColor': '#343434'
          }
        }
      },
      insights: {
        enabled: false
      }
    });
  }

  try {
    runFunction.$inject = ['$ionicPlatform', '$rootScope', '$state', 'NotificationsService'];
  } catch (e) {
    console.log( `Error setting up notifications (this is ok in the browser)` );
  }

  function runFunction($ionicPlatform, $rootScope, $state, NotificationsService) {
    $ionicPlatform.ready(function() {
    console.log( "READY READY READY");
    setupNotifications();

    cordova.plugins.intercom.registerIdentifiedUser({app_id: "rus6c6vh"});
    cordova.plugins.intercom.setLauncherVisibility('VISIBLE');
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

    $rootScope.$state = $state;

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
      // We can catch the error thrown when the $requireSignIn promise is rejected
      // and redirect the user back to the home page
      if (error === 'AUTH_REQUIRED') {
        $state.go('app.sign-in');
      }
    });

    function setupNotifications() {
      console.log('-----------ONE------------ ' );
      // alert(FCMPlugin);
      console.log('----------TWO-------------');
      FCMPlugin.onNotification(function(data){

        //For now just clear the badge whenever we get a notification.
        NotificationsService.clearIconBadge();

        if(data.wasTapped){
          //Notification was received on device tray and tapped by the user.
          console.log('************************');
          console.log( 'was Tapped ' + JSON.stringify(data));
          let jobId = data.jobId;
          let state = data.state;
          console.log( "====== " + state );
          let targetType = data.targetType;
          if( jobId && targetType === 'consumer') {
            console.log( 'Transitioning to CONSUMER detail');
            $state.go('app.consumer.job-detail', {
              id: jobId,
              state: state
            });
          } else if( jobId && targetType === 'panda') {
            console.log( 'Transitioning to PANDA detail');
            $state.go('app.panda.job-detail', {
              id: jobId,
              state: state
            });
          } else {
            console.log( 'Non-job notifications not yet supported' );
          }
          console.log('************************2');
        } else {
          //Notification was received in foreground. Maybe the user needs to be notified.
          console.log('************************');
          console.log( 'was not Tapped ' + JSON.stringify(data));
          let jobId = data.jobId;
          let state = data.state;
          console.log( '------ ' + state );
          let targetType = data.targetType;
          if( jobId && targetType === 'consumer') {
            console.log( 'Transitioning to CONSUMER detail');
            $state.go('app.consumer.job-detail', {
              id: jobId,
              state: state
            });
          } else if( jobId && targetType === 'panda') {
            console.log( 'Transitioning to PANDA detail');
            $state.go('app.panda.job-detail', {
              id: jobId,
              state: state
            });
          } else {
            console.log( 'Non-job notifications not yet supported' );
          }
          console.log('************************3');
        }
      });

      console.log('-----------THREE------------');
      FCMPlugin.subscribeToTopic('marketing');
      console.log('-----------FOUR------------');

      FCMPlugin.onTokenRefresh().subscribe(token => {
        console.log( "TOKEN REFRESH");
      });

    }
  }

})();
