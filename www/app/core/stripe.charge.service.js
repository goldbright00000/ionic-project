(function() {
	'use strict';


	angular
		.module('app.core')
		.factory('StripeCharge', StripeCharge);

	StripeCharge.$inject = ['$firebaseObject', 'FirebaseRef', '$q', '$http', 'StripeCheckout', 'JobsService', 'CacheService', 'stripeKey'];

	function StripeCharge($firebaseObject, FirebaseRef, $q, $http, StripeCheckout, JobsService, CacheService, stripeKey) {

    const serverURL = CacheService.getServerURL()
    const chargeServerUrl = serverURL + 'charge';
    const getAcctIdServerUrl = serverURL + 'fetchChoreAcctId';
    const getCryptoSecretUrl = serverURL + 'fetchCryptoSecret';

		var accountId = null;
		var cryptoSecret = null;

		var service = {
			chargeUser: chargeUser,
			getStripeToken: getStripeToken,
      getAccountId: getAccountId,
      getCryptoSecret: getCryptoSecret
		};

    fetchAccountId();
    fetchCryptoSecret();
		console.log( `--------> ${stripeKey.key}` );

		return service;


		//  var self = this;

		/**
		 * Connects with the backend (server-side) to charge the customer
		 *
		 * # Note on the determination of the price
		 * In this example we base the $stripeAmount on the object ProductMeta which has been
		 * retrieved on the client-side. For safety reasons however, it is recommended to
		 * retrieve the price from the back-end (thus the server-side). In this way the client
		 * cannot write his own application and choose a price that he/she prefers
		 */


		function chargeUser(stripeToken, ProductMeta, jobId) {
        console.log(ProductMeta);
        var qCharge = $q.defer();
        var job;

        JobsService.getJob(jobId).$loaded(function(res) {
          job = res;

          var curlData = {
            stripeCurrency: "usd",
            stripeAmount: Math.floor(ProductMeta.priceUSD * 100),
            stripeSource: stripeToken,
            stripeDescription: `Payment for job ${job.type}`,
            pandaIds: job.pandasAccepted,
            jobId: job.$id
        //  stripeDestinationAccountId: ProductMeta.productUserId
          };

          curlData["stripeDestinationAccountId"] = ProductMeta.productUserId;

          // console.log(curlData);
          // console.log(chargeServerUrl);
          $http.post(chargeServerUrl, curlData)
            .success(function(StripeInvoiceData) {
                console.log(StripeInvoiceData);
                if(StripeInvoiceData != null && StripeInvoiceData.hasOwnProperty('status')) {

                  if(StripeInvoiceData.status === "succeeded") {
                    qCharge.resolve(StripeInvoiceData);
                  }
                  qCharge.reject(StripeInvoiceData);
                } else {
                  qCharge.reject(StripeInvoiceData);
                }
              })
            .error(
              function(error) {
                console.log(error)
                qCharge.reject(error);
              }
            );
        });
        return qCharge.promise;
			}

		/**
		 * Get a stripe token through the checkout handler
		 */
		function getStripeToken(ProductMeta) {
//      var stripe = Stripe('pk_live_KowOVeaKH9nz1drjfGB0pk0f')
      var stripe = Stripe('pk_test_j4nJLAdxhucNlggWCKFSpEVs')

      // var cardElement = stripe.elements().create('card', {
      //   style: {
      //     base: {
      //       color: '#303238',
      //       fontSize: '16px',
      //       lineHeight: '24px',
      //       fontSmoothing: 'antialiased',
      //       '::placeholder': {
      //         color: '#ccc',
      //       },
      //     },
      //     complete: {
      //       color: '#363'
      //     }
      //   }
      // });


			//self.getStripeToken = function(ProductMeta) {
			var qToken = $q.defer();

			var handlerOptions = {
				name: ProductMeta.name,
				description: ProductMeta.description,
				amount: Math.floor(ProductMeta.priceUSD * 100),
				// image: "img/chorelogo.png",
			};

			var handler = StripeCheckout.configure({
				name: ProductMeta.name,
				panelLabel: ProductMeta.panelLabel,
				image: ProductMeta.image,
				key: stripeKey.key,
        amount: Math.floor(ProductMeta.priceUSD * 100),
        // options: {
  			// 	name: ProductMeta.title,
  			// 	description: ProductMeta.description,
        //
  			// 	amount: Math.floor(ProductMeta.priceUSD * 100),
  			//  // image: "img/chorelogo.png",
  			// },
				token: function(token, args) {
					//console.log(token.id)
				}
			});

      handler.open(handlerOptions).then(
            function(result) {
              var stripeToken = result[0].id;
              if(stripeToken != undefined && stripeToken != null && stripeToken != "") {
                  //console.log("handler success - defined")
                  qToken.resolve(stripeToken);
              } else {
                  //console.log("handler success - undefined")
                  qToken.reject("ERROR_STRIPETOKEN_UNDEFINED");
              }
            }, function(error) {
              if(error == undefined) {
                  qToken.reject("ERROR_CANCEL");
              } else {
                  qToken.reject(error);
              }
            } // ./ error
          ); // ./ handler

			// var myPopup = $ionicPopup.show({
			// 	template: '<div id="card-element"></div>'
			// })
      //
      // setTimeout(function() {
      //   cardElement.mount('#card-element');
      // }, 3000)
			return qToken.promise;
		};

    function fetchAccountId() {
      $http.get(getAcctIdServerUrl).success(function( res ) {
        console.log( `Fetched account :${res}` );
        accountId = res.id;
      })
        .error(function(error) {
            console.log( '----------------------------------------' );
            console.log("Unable to retrieve account id from server");
            console.log(error);
            console.log( '----------------------------------------' );
          }
        );
    }

    function fetchCryptoSecret() {
      $http.get(getCryptoSecretUrl).success(function( res ) {
        // console.log( `Fetched key :${res}` );
        cryptoSecret = res.id;
      })
        .error(function(error) {
            console.log( '----------------------------------------' );
            console.log("Unable to retrieve account id from server");
            console.log(error);
            console.log( '----------------------------------------' );
          }
        );
    }

    function getAccountId() {
      return accountId;
    }

    function getCryptoSecret() {
      return cryptoSecret;
    }

}})();
