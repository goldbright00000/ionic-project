(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('ProfileService', ProfileService);

    ProfileService.$inject = ['$firebaseObject', 'FirebaseRef'];

    function ProfileService($firebaseObject, FirebaseRef) {

      var user = {};

        var service = {
            fetchProfile: fetchProfile,
            setUser: setUser,
            getUser: getUser,
            isUserSet: isUserSet
        };

        return service;

        ////////////

        function fetchProfile(uid) {
            var profileRef = FirebaseRef.profiles.child(uid);
            return $firebaseObject(profileRef);
        }

        function setUser(newUser){
          user = newUser;
        }

        function getUser() {
          return user;
        }

        function isUserSet() {
          return Object.keys(user).length !== 0;
        }
    }
})();
