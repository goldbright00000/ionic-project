(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('ChatService', ChatService);

    ChatService.$inject = ['$firebaseObject', '$firebaseArray', 'FirebaseRef'];

    function ChatService($firebaseObject, $firebaseArray, FirebaseRef) {

        var service = {
            getChatRoom: getChatRoom,
            getChatMessages: getChatMessages
        };

        return service;

        ////////////

        function getChatRoom(jobId, roomId) {
            var roomRef = FirebaseRef.jobChatRooms.child(jobId + '/' + roomId);
            console.log( jobId + " " + roomId );
            return $firebaseObject(roomRef);
        }

        function getChatMessages(id) {
            var messageRef = FirebaseRef.chatMessages.child(id);
            return $firebaseArray(messageRef);
        }
    }
})();
