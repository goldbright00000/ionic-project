(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('JobsService', JobsService);

    JobsService.$inject = ['$firebaseObject', '$firebaseArray', 'FirebaseRef', 'ProfileService', '$rootScope', '$timeout'];

    function JobsService($firebaseObject, $firebaseArray, FirebaseRef, ProfileService, $rootScope, $timeout) {

      console.log('******* -- JobsService -- ************** ');

      var jobHistory = {}
      var currentJobList = {};

        var homeJobTypes = [{
            title: 'Painting Inside',
            id: 3,
            iconClass: 'ion-paintbrush'
          },
          {
            title: 'Painting Outside',
            id: 2,
            iconClass: 'ion-paintbucket'
          },
          {
            title: 'Yard Work',
            id: 1,
            iconClass: 'ion-leaf'
          },
          {
            title: 'Home Cleaning',
            id: 4,
            iconClass: 'ion-ios-home'
          },
          {
            title: 'Furniture Assembly',
            id: 5,
            iconClass: 'ion-settings'
          },
          {
            title: 'Moving and Packing',
            id: 6,
            iconClass: 'ion-cube'
          },
          {
            title: 'Car/Boat Wash',
            id: 7,
            iconClass: 'ion-model-s'
          },
          {
            title: 'Loading and Unloading',
            id: 8,
            iconClass: 'ion-ios-box'
          },
          {
            title: 'Organizing',
            id: 9,
            iconClass: 'ion-filing'
          },
          {
            title: 'Laundry',
            id: 10,
            iconClass: 'ion-tshirt'
          },
        //  {
        //    title: 'Running Errands',
        //    id: 11,
        //    iconClass: 'ion-ios-list'
        //  },
          {
            title: 'Window Washing',
            id: 12,
            iconClass: 'ion-waterdrop'
          },
          {
            title: 'Extra Hand',
            id: 13,
            iconClass: 'ion-android-hand'
          }
        ];

        var businessJobTypes = [{
            title: 'Painting Inside',
            id: 1,
            iconClass: 'ion-paintbrush'
          },
          {
            title: 'Painting Outside',
            id: 2,
            iconClass: 'ion-paintbucket'
          },
        //  {
        //    title: 'Yard Work',
        //    id: 3,
        //    iconClass: 'ion-hammer'
      //    },
      //    {
    //        title: 'Home Cleaning',
    //        id: 4,
    //        iconClass: 'ion-ios-home'
      //    },
          {
            title: 'Furniture Assembly',
            id: 5,
            iconClass: 'ion-settings'
          },
          {
            title: 'Moving and Packing',
            id: 6,
            iconClass: 'ion-cube'
          },
      //    {
      //      title: 'Car/Boat Wash',
      //      id: 7,
    //        iconClass: 'ion-model-s'
      //    },
          {
            title: 'Loading and Unloading',
            id: 8,
            iconClass: 'ion-ios-box'
          },
          {
            title: 'Organizing',
            id: 9,
            iconClass: 'ion-filing'
          },
      //    {
      //      title: 'Laundry',
      //      id: 10,
      //      iconClass: 'ion-tshirt'
    //      },
    //      {
    //        title: 'Running Errands',
  //          id: 11,
    //        iconClass: 'ion-ios-list'
    //      },
          {
            title: 'Window Washing',
            id: 12,
            iconClass: 'ion-waterdrop'
          },
          {
            title: 'Extra Hand',
            id: 13,
            iconClass: 'ion-android-hand'
          }
        ];

        var service = {
            getJob: getJob,
            getJobPandas: getJobPandas,
            getJobTypes: getJobTypes,
            getCurrentJobList: getCurrentJobList,
            getJobHistory: getJobHistory,
            updateJobHistory: updateJobHistory
        };

        subscribeToOpenJobs();
        fetchJobHistory();

      return service;

        ////////////

      function getJobHistory() {
        return jobHistory;
      }

      function getCurrentJobList() {
        return Object.values(currentJobList);
      }

      function updateJobHistory(id, job) {
        jobHistory[id] = job;
      }

        function getJob(id) {
            const jobRef = FirebaseRef.jobListings().child(id);
            return $firebaseObject(jobRef);
        }

        //This uses a watched array to ensure that it one worker books a job while the other is looking at it
        //the second to apply gets denied
        function getJobPandas(id) {
            const jobRef = FirebaseRef.jobListings().child(id + '/pandasAccepted');
            return $firebaseArray(jobRef);
         }

        function getJobTypes(category) {
            return category === 'home' ? homeJobTypes : businessJobTypes;
        }

        //Get open jobs that have not expired and for which the supplied panda has not signed up
        function subscribeToOpenJobs() {
          FirebaseRef.jobListings().orderByChild('completed').equalTo(false)
            .on('child_added', function(snapshot) {
              const job = snapshot.val();
              if( isListableJob(job) ) {
                job.key = snapshot.key;
                currentJobList[snapshot.key] = job;
                console.log(`Job Add ${snapshot.val().type}`);
                $rootScope.$broadcast('jobListUpdated');
              }
            });

          FirebaseRef.jobListings().on('child_changed', function(childSnapshot, prevChildKey) {
            console.log(`DATA CHANGED ${childSnapshot.val().type}`);
            const updatedJob = childSnapshot.val();
            const key = childSnapshot.key;

            if( !isListableJob(updatedJob) ) {
              console.log(`Job remove ${updatedJob.type}`);
              delete currentJobList[childSnapshot.key];
            } else {
              updatedJob.key = childSnapshot.key;
              currentJobList[childSnapshot.key] = updatedJob;
            }

            $rootScope.$broadcast('jobListUpdated');
          });


          // Since we return a static array we don't get real time updates
          // return FirebaseRef.jobListings().orderByChild('hasEnoughPandas').equalTo(false);
        }

        function isListableJob(job) {
          const targetId = ProfileService.getUser().$id;
          const now = Date.now();

          return !job.cancelled  //not cancelled
            && !job.completed
            && job.endTime > now //not expired
            && !job.hasEnoughPandas
            //not enough pandas and I'm not one of the pandas
            && (!job.pandasAccepted || (job.pandasAccepted && !Object.keys(job.pandasAccepted).includes(targetId )));
        }

        //Fetch the jobs in the current user's profile
        function fetchJobHistory() {
          jobHistory = {};
          const uid = ProfileService.getUser().$id;
          // var indexRef = FirebaseRef.profiles.child(user.uid + '/jobListings').limitToLast(15);
            var indexRef = FirebaseRef.profiles.child(uid + '/jobListings').limitToLast(30);

          // watch the user's jobs index for add events
          indexRef.on('child_added', function(indexSnap) {
            // fetch the job and put it into our list
            var jobListingId = indexSnap.key;

            FirebaseRef.jobListings().child(jobListingId).once('value', function(snap) {
              // trigger $digest/$apply so Angular syncs the DOM
              $timeout(function() {
                if( snap.val() === null ) {
                  // the job was deleted
                  delete jobHistory[jobListingId];
                }
                else {
                  jobHistory[jobListingId] = snap.val();
                }
            });
          });
        });
      }

    }
})();
