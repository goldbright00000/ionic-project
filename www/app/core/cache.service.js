(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('CacheService', CacheService);

    const prodURL = 'https://chore-connect.herokuapp.com/';
    // const prodURL = 'http://localhost:9311/';
    const qaURL = 'https://chore-connect-qa.herokuapp.com/';

    let serverURL = prodURL;
    // const serverURL = 'http://192.168.0.14:9311/';
    CacheService.$inject = [];

    function CacheService() {
      let chore = '';
      let token = null;
      let mode = 'prod';
      
      let setChore = function(name) {
        chore = name;
      };

      let getChore = function(){
        return chore;
      };

      let setToken = function(newToken) {
        console.log(`Saving token ${newToken}`);
        token = newToken;
      };

      let getToken = function(){
        return chore;
      };

      let setServerToProd = function() {
        serverURL = prodURL;
        mode = 'prod';
      }

      let setServerToQA = function() {
        serverURL = qaURL;
        mode = 'qa';
      }

      let getServerURL = function () {
        return serverURL;
      }

      let getMode = function()  {
        return mode;
      }

      return {
        setServerToProd: setServerToProd,
        setServerToQA: setServerToQA,
        setChore: setChore,
        getChore: getChore,
        setToken: setToken,
        getToken: getToken,
        getServerURL: getServerURL,
        getMode: getMode,
      };
    }
})();
