(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('NotificationsService', NotificationsService);

  NotificationsService.$inject = ['$http', 'FirebaseRef', 'CacheService'];

  function NotificationsService($http, FirebaseRef, CacheService) {

    let jobDetailState = {
      SHOW_JOB: 'SHOW_JOB',
      REQUEST_TO_START: 'REQUEST_TO_START',
      APPROVE_REQUEST_TO_START: 'APPROVE_REQUEST_TO_START',
      RATE_USER: 'RATE_USER',
    };

    let service = {
      sendNotificationToConsumer: sendNotificationToConsumer,
      sendNotificationToPanda: sendNotificationToPanda,
      clearIconBadge: clearIconBadge,
      jobDetailState: jobDetailState
    };

    return service;


    //TODO Reduce duplicate code
    function sendNotificationToConsumer(job, title, body, state) {
      // Get the job poster's device token
      FirebaseRef.deviceTokens.child(job.createdBy).once('value').then(function (snapshot) {
        let token = snapshot.val();

        let payload = {
          'notification': {
            'body': body,
            'title': title
          },
          'data': {
            'jobId': job.$id,
            'targetType': 'consumer',
            'state': state
          },
          'apns': {
            'payload': {
              'aps': {
                'badge': 1,
                'sound': 'default'
              }
            }
          },
          'android': {
            'ttl': 60000,
            'priority': 'high'
          },

          'token': token
        };

        const serverURL = CacheService.getServerURL();
        const notificationURL = serverURL + 'notification/send';

        // Send the job poster a push notification via the Ionic Cloud Services API
        $http.post(notificationURL, payload)
          .then(function (res) {
            console.log('Successfully sent push notification to job poster.');
          }).catch(function (err) {
          console.log('Error sending push notification to customer: ' + err);
        });
      });
    }

    function sendNotificationToPanda( job,  title, body, state) {
      if( !job.pandasAccepted ) {
        return;
      }

      // Get the job posters device tokens
      var keys = Object.keys(job.pandasAccepted);
      keys.forEach( (pandaId) => {
        FirebaseRef.deviceTokens.child(pandaId).once('value').then(function (snapshot) {
          let token = snapshot.val();

          let payload = {
            'notification': {
              'body': body,
              'title': title
            },
            'data': {
              'jobId': job.$id,
              'targetType': 'panda',
              'state': state
            },
            'apns': {
              'payload': {
                'aps': {
                  'badge': 1,
                  'sound': 'default'
                }
              }
            },
            'android': {
              'ttl': 60000,
              'priority': 'high'
            },

            'token': token
          };

          const serverURL = CacheService.getServerURL();
          const notificationURL = serverURL + 'notification/send';

          // Send the job poster a push notification via the Ionic Cloud Services API
          $http.post(notificationURL, payload)
            .then(function (res) {
              console.log(`Successfully sent push notification to panda ${pandaId}.`);
            }).catch(function (err) {
            console.log('Error sending push notification to worker: ' + err);
          });
      });
    });
    }

    function clearIconBadge() {
      try{
        // cordova.plugins.notification.badge.set(3);
        cordova.plugins.notification.badge.clear();
      }
      catch (e) {
        console.log( 'Could not clear icon badge' );
      }
    }
  }
})();
