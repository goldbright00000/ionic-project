(function() {
    'use strict';

    angular
        .module('app.core')
        .service('ModalService', ModalService);

    ModalService.$inject = ['$ionicModal', '$rootScope'];

    function ModalService($ionicModal, $rootScope) {

      var userPressedOK = function(modal) {
        modal.hide();
      };

      var applicationDetail = function(tpl, $scope) {
        var promise;
        $scope = $scope || $rootScope.$new();

        promise = $ionicModal.fromTemplateUrl(tpl, {
          scope: $scope,
          animation: 'slide-in-up',
        }).then(function(modal) {
          $scope.helpModal = modal;
          return modal;
        });

        $scope.closeModal = function() {
          $scope.help = true;
          $scope.helpModal.hide();

        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.helpModal.remove();
          $scope.help = true;

        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function() {
          $scope.help = true;
          // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function() {
          $scope.help = true;
          // Execute action
        });

      return promise;
    };

    return {
      applicationDetail: applicationDetail,
      userPressedOK: userPressedOK
    };

  }
})();
