(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('FirebaseRef', ['CacheService', FirebaseRef]);

    function FirebaseRef(CacheService) {
        var root = firebase.database().ref();
//        root.setLogLevel(Logger.Level.INFO);

        console.log( '*********************** ' + CacheService.getServerURL());

        let jobListings = function() {
          if( CacheService.getMode() === 'qa' ) {
            console.log('--- testJobListings ---');
            return root.child('testJobListings')
          } else {
            console.log('--- jobListings ---');
            return root.child('jobListings')
          }
        }

        var service = {
            root: root,
            profiles: root.child('profiles'),
            deviceTokens: root.child('deviceTokens'),
            chatRooms: root.child('chatRooms'),
            jobChatRooms: root.child('jobChatRooms'),
            chatMessages: root.child('chatMessages'),
            jobListings: jobListings,
            jobApplications: root.child('jobApplications'),
            pendingTransactions: root.child('pendingTransactions')
        };

        return service;
    }
})();
