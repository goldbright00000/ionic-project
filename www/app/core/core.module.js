(function() {
  'use strict';

  const app = angular.module('app.core', ['ionic', 'ionic-datepicker', 'ionic-timepicker']);

  app.filter('rounded', function(){
    return function(val,to){
      if( !val ) { return null; }

      return val.toFixed(to || 0);
    };
  });

})();
