(function() {
    'use strict';

    var SERVER_SIDE_URL =  "http://192.168.0.3:9311/";
    //  var STRIPE_API_PUBLISHABLE_KEY  = "pk_test_yzeec730LKSaj0wmqrYkJmER"
    // var STRIPE_API_PUBLISHABLE_KEY  = "pk_test_j4nJLAdxhucNlggWCKFSpEVs"

    angular
        .module('app.core')
        .factory('StripeService', StripeService);

    StripeService.$inject = ['$firebaseObject', 'FirebaseRef', '$q', '$http'];

    function StripeService($firebaseObject, FirebaseRef, $q, $http) {

        var service = {
            createAccount: createAccount,
            generateFBAuthToken: generateFBAuthToken
        };

        return service;

        ////////////

        function createAccount() {
          self.chargeUser = function(stripeToken, ProductMeta) {
          var qCharge = $q.defer();

          var chargeUrl = SERVER_SIDE_URL + "/create";
          var curlData = {
            stripeCurrency:         "usd",
            stripeAmount:           Math.floor(ProductMeta.priceUSD*100),  // charge handles transactions in cents
            stripeSource:           stripeToken,
            stripeDescription:      "Your custom description here"
          };
          $http.post(chargeUrl, curlData)
          .success(
            function(StripeInvoiceData){
              qCharge.resolve(StripeInvoiceData);
              // you can store the StripeInvoiceData for your own administration
              console.log(StripeInvoiceData);
            }
          )
          .error(
            function(error){
              console.log(error)
              qCharge.reject(error);
            }
          );
          return qCharge.promise;
        };
        }

        function generateFBAuthToken(userId){
          console.log(userId);
            // var STRIPE_FIREBASE_GEN_TOKEN  = 'https://purplepandastripeconnect.herokuapp.com/' + "/firebase/generatetoken";
            var STRIPE_FIREBASE_GEN_TOKEN  = 'https://192.168.0.3:9311/' + "/firebase/generatetoken";
            var qGen = $q.defer();
            //console.log("requesting to: " + STRIPE_FIREBASE_GEN_TOKEN);
            $http.post(STRIPE_FIREBASE_GEN_TOKEN, {userId: userId})
            .success(
                function(fbAuthToken){
                  if(fbAuthToken != null) {
                      qGen.resolve(fbAuthToken);
                  } else {
                      qGen.reject("ERROR_NULL");
                  }
          } )
            .error(
                function(error){
                    qGen.reject(error);
                }
          );
            return qGen.promise;
          };

              }
})();
