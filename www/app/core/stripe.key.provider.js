(function() {
  'use strict';
  let TEST_STRIPE_API_PUBLISHABLE_KEY = 'pk_test_j4nJLAdxhucNlggWCKFSpEVs';
  let LIVE_STRIPE_API_PUBLISHABLE_KEY = "pk_live_gLPRTnMRkbMIaHb1eujG4epH"

  angular
    .module('app.core')
    .provider("stripeKey", function () {
      var type;
      return {
        $get: function (CacheService) {
          let key = (CacheService.getMode() === 'qa')
                ? TEST_STRIPE_API_PUBLISHABLE_KEY
                : LIVE_STRIPE_API_PUBLISHABLE_KEY;
          return {
            key: key
          };
        }
      };
    });
})();
