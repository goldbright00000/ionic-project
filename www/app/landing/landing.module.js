(function() {
  'use strict';

  angular.module('app.landing', ['ionic']).controller('LandingController', LandingController);
  LandingController.$inject = [
    '$scope',
    '$state',
    'Auth',
    'FirebaseRef',
    '$firebaseObject',
    '$ionicHistory',
    '$ionicPush',
    '$rootScope',
    'ProfileService',
    'CacheService',
    '$ionicModal',
  ];

  function LandingController($scope, $state, Auth, FirebaseRef, $firebaseObject, $ionicHistory, $ionicPush, $rootScope, ProfileService, CacheService, $ionicModal) {
    let TEST_STRIPE_API_PUBLISHABLE_KEY = 'pk_test_j4nJLAdxhucNlggWCKFSpEVs';

    const ppuid = localStorage.getItem("ppuid");

    if (ppuid && localStorage.getItem("role")) {
      // $scope.showSpinner = true;
      var uid = localStorage.getItem("ppuid");
      var role = localStorage.getItem("role");
      // User logged in, get user object from db
      console.log('Logging in user from loca storage: ' + uid);
      $rootScope.isLoggedIn = true;

      if( ProfileService.isUserSet() ) {
        let prof = ProfileService.getUser(uid);
        transitionToHomePage(prof);
      } else {
        var profileRef = FirebaseRef.profiles.child(uid);
        $firebaseObject(profileRef).$loaded( function(profile) {
            // console.log(profile);
            fetchAndSaveFCMToken(profile.$id);
            transitionToHomePage(profile);
        }, function(error) {
            console.log('Error getting users: ' + error);
        });
      }
    } else {
      console.log('No user found in local storage');
    }

    if (localStorage.getItem("mode")) {
      const mode = localStorage.getItem("mode");
      if( mode === 'qa' ) {
        console.log( 'Setting to QA Mode' );
        CacheService.setServerToQA();
      } else {
        console.log( 'Setting to Prod Mode' );
        CacheService.setServerToProd();
      }
    }

    function transitionToHomePage(profile) {
      $ionicHistory.nextViewOptions({
          historyRoot: true
      });
      if (profile.role === 'consumer') {
          $state.go('app.consumer.home');
      } else {
          $state.go('app.panda.jobs-list');
      }
      // $scope.showSpinner = false;
    }
    //TODO this code is duplicated in the sign-in controller :-(
    function fetchAndSaveFCMToken(uid) {
      try {
        FCMPlugin.getToken ( //TODO consider saving token to local storage?
          function (token) {
            console.log('Token: ' + token);
            CacheService.setToken(token);
            FirebaseRef.deviceTokens.child(uid).set(token);
          },
          function (err) {
            // alert('failed to get token: ' + token);
            console.log('error retrieving token: ' + err);
          });
      } catch(err) {
        console.log( `Error fetching token ${err}` );
      }
    }

    $scope.onHold = function() {
      console.log( "onHold");
    }

    $scope.pressed = function() {
      console.log( "pressed");
    }

    $scope.serverPick = function() {
      console.log( "SERVER PICK");
      $scope.modalModel = {'server':null};

      $ionicModal.fromTemplateUrl('app/landing/serverPick.html', {
        scope: $scope,
        animation: 'slide-in-up',
      }).then(function(modal) {
        $scope.serverPickModal = modal;
        $scope.serverPickModal.show();
        return modal;
      });
    }

    $scope.serverOK = function(res) {
      $scope.serverPickModal.hide();
      if( $scope.modalModel.modalPassword === '331122' ) {
        if( $scope.modalModel.server === 'qa' ) {
          console.log( 'Setting to QA server' )
          CacheService.setServerToQA();
          localStorage.setItem('mode', 'qa');
        } else {
          console.log( 'Setting to Prod server' )
          CacheService.setServerToProd();
          localStorage.setItem('mode', 'prod');
        }
      }
    }

  }
})();
