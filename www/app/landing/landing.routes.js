(function() {
    'use strict';

    angular
      .module('app.landing')
      .config(configFunction);

    configFunction.$inject = ['$stateProvider'];

    function configFunction($stateProvider) {
        $stateProvider
            .state('app.landing', {
                url: '/landing',
                views: {
                    'menuContent': {
                        templateUrl: 'app/landing/landing.html',
                        controller: 'LandingController'
                    }
                }
            })

    }
})();
