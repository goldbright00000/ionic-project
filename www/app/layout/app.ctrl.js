(function() {
    'use strict';

    angular
    .module('purplePanda')
    .controller('AppCtrl', AppCtrl);

    AppCtrl.$inject = [
      '$scope',
      '$state',
      'Auth',
      'FirebaseRef',
      'ProfileService',
      '$timeout',
      '$firebaseArray',
      '$ionicSideMenuDelegate',
      '$ionicPush',
      '$ionicHistory',
      '$rootScope',
      'CacheService'
    ];

    function AppCtrl($scope, $state, Auth, FirebaseRef, ProfileService, $timeout, $firebaseArray, $ionicSideMenuDelegate, $ionicPush, $ionicHistory, $rootScope, CacheService) {

        $scope.profile = null;
        $scope.jobListings = {};

/*      Helpful for debuggin routing issues
        $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
          console.log('$stateChangeStart from '+fromState.name+'- fired when the transition begins. fromParams,toParams : \n',fromState, fromParams);
          console.log('$stateChangeStart to '+toState.name+'- fired when the transition begins. toState,toParams : \n',toState, toParams);
        });
*/

        $scope.$watch(function () {
            return $ionicSideMenuDelegate.getOpenRatio();
        },
                      function (ratio) {
          if (ratio === 1){
              $scope.jobsMenu = false;
          }
        });

        $scope.toggleJobsMenu = function() {
          $scope.jobsMenu = !$scope.jobsMenu;
        };

        // On login,
        Auth.firebaseAuthObject.$onAuthStateChanged(function(user) {
            // console.log($scope.profile);
            // console.log('Firebase user: ' + !!user);
            $scope.qaMode = CacheService.getMode() === 'qa';

            if (user) {
              $scope.profile = ProfileService.fetchProfile(user.uid);
              $scope.profile.$loaded().then(function() {
                ProfileService.setUser($scope.profile);
                // if ($scope.profile.role === 'panda') {
                //   getPandaJobs(user);
                // } else {
                //   // getConsumerJobs(user);
                // }
              });
            }
             else {
                console.log('User not logged in');
            }
        });

        window.blah = $scope.jobs;
        function getPandaJobs(user) {
            var indexRef = FirebaseRef.profiles.child(user.uid + '/jobListings').limitToLast(10);

            // watch the user's jobs index for add events
            indexRef.on('child_added', function(indexSnap) {
                 // fetch the job and put it into our list
                 var jobListingId = indexSnap.key;

                 FirebaseRef.jobListings().child(jobListingId).on('value', function(snap) {
                      // trigger $digest/$apply so Angular syncs the DOM
                      $timeout(function() {
                           if( snap.val() === null ) {
                                // the job was deleted
                                delete $scope.jobListings[jobListingId];
                           }
                           else {
                                $scope.jobListings[jobListingId] = snap.val();
                           }
                      });
                 });
            });

            // watch the index for remove events
            indexRef.on('child_removed', function(snap) {
                 // trigger $digest/$apply so Angular updates the DOM
                 $timeout(function() {
                      delete $scope.jobListings[snap.key];
                 });
            });
        }







      /**
       * For the sake of efficiency this code largely duplicates getPandaJobs but does not subscribe to
       * changes in the DB. This is to save push traffic when a job changes.
       * @param user
       */
      function getConsumerJobs(user) {
        // var indexRef = FirebaseRef.profiles.child(user.uid + '/jobListings').limitToLast(15);
          var indexRef = FirebaseRef.profiles.child(user.uid + '/jobListings').limitToLast(30);

        // watch the user's jobs index for add events
        indexRef.on('child_added', function(indexSnap) {
          // fetch the job and put it into our list
          var jobListingId = indexSnap.key;

          FirebaseRef.jobListings().child(jobListingId).once('value', function(snap) {
            // trigger $digest/$apply so Angular syncs the DOM
            $timeout(function() {
              if( snap.val() === null ) {
                // the job was deleted
                delete $scope.jobListings[jobListingId];
              }
              else {
                $scope.jobListings[jobListingId] = snap.val();
              }
            });
          });
        });
      }


      $scope.isSignedIn = function() {
          console.log( `====> ${localStorage.getItem("role") !== null}`);
          return localStorage.getItem("role") !== null;
      };

      $scope.signOut = function() {
        localStorage.removeItem('role');
        localStorage.removeItem('ppuid');
        if( window.hasOwnProperty('cordova') ) { //are we on a device?
          FirebaseRef.deviceTokens.child($scope.profile.$id).remove();
        }

        firebase.database().goOffline();

        $ionicPush.unregister()
            .then(Auth.logout()
                .then(function() {
                    $scope.profile = null;
                    $scope.jobListings = {};

                    $ionicHistory.nextViewOptions({
                        historyRoot: true
                    });
                    $state.go('app.landing');
                    console.log('in signOut()');
                })
                .catch(function(error) {
                    // An error happened.
                    console.log("Error logging out: " + error.code + " - " + error.message);
                })
            ).catch(function(error) {
                // An error happened.
                console.log("Error unregistering device: " + error);
            });
      };
    }
})();
