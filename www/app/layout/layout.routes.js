(function() {
    'use strict';

    angular
      .module('app.layout')
      .config(configFunction);

    configFunction.$inject = ['$stateProvider'];

    function configFunction($stateProvider) {
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'app/layout/menu.html',
                controller: 'AppCtrl'
            })
            .state('app.terms', {
                url: '/terms',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/layout/terms.html'
                    }
                }
            })
            .state('app.privacy', {
                url: '/privacy',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/layout/privacy.html'
                    }
                }
            })
            .state('app.about', {
                url: '/about',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/layout/about.html'
                    }
                }
            });
    }
})();
