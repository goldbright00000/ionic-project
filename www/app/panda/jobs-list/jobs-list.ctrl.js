(function() {
  'use strict';

  angular
    .module('app.panda')
    .controller('JobsListCtrl', JobsListCtrl);

  JobsListCtrl.$inject = [
    '$scope',
    '$state',
    'Auth',
    'currentAuth',
    'FirebaseRef',
    '$firebaseArray',
    '$ionicHistory',
    'JobsService',
    '$rootScope',
    'ProfileService'
  ];

  function JobsListCtrl($scope, $state, Auth, currentAuth, FirebaseRef, $firebaseArray, $ionicHistory, JobsService, $rootScope, ProfileService) {
    

    $rootScope.$on('jobListUpdated', function(event,data) {
      console.log( 'update in panda job list controller' );
      refreshList();
    });

    //This code fixes an issue where the navigation got confused when a panda booked a job
    $rootScope.$on('$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams) {
        if( toState.name === 'app.panda.jobs-list') {
          console.log( 'Clearing view cache');
          $ionicHistory.clearCache();
        }
        // console.log( `From: ${fromState.name} To:${toState.name}`)
        // console.log($ionicHistory.viewHistory());
      });


    window.Intercom('boot', {
      app_id: 'rus6c6vh',
      email: currentAuth.email, // Email address
      custom_launcher_selector: '#my_custom_link'
    });

    // $scope.$on('$stateChangeSuccess', function()
    // {
    //   refreshList();
    // });

    function refreshList() {
      if( $scope.user && $scope.user.$id && $scope.user.active ) {
        $scope.availableJobs = JobsService.getCurrentJobList();
      }
      $scope.$apply();
    }

    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

    // TODO Filter available jobs by search radius, etc
    // $scope.availableJobs = $firebaseArray(FirebaseRef.jobListings().orderByChild('hasEnoughPandas').equalTo(false));
    // $scope.availableJobs = $firebaseArray(JobsService.getOpenJobs());
    // console.log( "BLOOP")
    $scope.user = ProfileService.getUser();
    if( !$scope.user ) {
      $scope.pandaActive = false;
      console.log( '-----------------------' );
      console.log( `Unable to get user for ${user}` );
      console.log( '-----------------------' );
    } else {
      if( $scope.user.$id ) {
        //Allow the jobs list to load for inactive pandas, we filter it out of the GUI later
        // $scope.availableJobs = JobsService.getOpenJobs();
      }
    }

    $scope.availableJobs = JobsService.getCurrentJobList();

  }
})();
