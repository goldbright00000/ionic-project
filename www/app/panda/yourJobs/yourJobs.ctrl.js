(function() {
  'use strict';

  angular
    .module('app.panda')
    .controller('yourJobsCtrl', yourJobsCtrl);

  yourJobsCtrl.$inject = [
    '$scope',
    '$state',
    'Auth',
    'currentAuth',
    'FirebaseRef',
    '$firebaseArray',
    '$ionicHistory',
    'JobsService',
    '$rootScope',
    'ProfileService'
  ];

  function yourJobsCtrl($scope, $state, Auth, currentAuth, FirebaseRef, $firebaseArray, $ionicHistory, JobsService, $rootScope, ProfileService) {

    $scope.$on('jobListUpdated', function(event,data) {
      console.log( 'update in panda your jobs controller' );
      // refreshList();
    });

      window.Intercom('boot', {
      app_id: 'rus6c6vh',
      email: currentAuth.email, // Email address
      custom_launcher_selector: '#my_custom_link'
    });

    // $scope.$on('$stateChangeSuccess', function()
    // {
    //   refreshList();
    // });


/////commented out refresh list below I dont think the fuction is necessary////

    // function refreshList() {
    //   if( $scope.user && $scope.user.$id && $scope.user.active ) {
    //     $scope.availableJobs = JobsService.getCurrentJobList();
    //     $scope.availableJobs.forEach( (job) => {
    //       console.log(`${job.type} !`);
    //     });
    //     Auth.firebaseAuthObject.$onAuthStateChanged(function(user) {
    //         // console.log($scope.profile);
    //         // console.log('Firebase user: ' + !!user);
    //
    //         if (user) {
    //            console.log( `--------mattt---------`);
    //             getUserJobs(user);
    //         }
    //          else {
    //             console.log('User not logged in');
    //         }
    //     });
    //
    //     console.log( `--------asd----${$scope.availableJobs[0]}---------`);
    //     // console.log($scope.profile.jobListings);
    //     console.log(Object.keys($scope.profile.jobListings));
    //     console.log($scope.availableJobs);
    //
    //
    //   }
    //   $scope.$apply();
    // }

    /////commented out getUserJobs I dont think the fuction is necessary////


    // function getUserJobs(user) {
    //
    //     var indexRef = FirebaseRef.profiles.child(user.uid + '/jobListings').limitToLast(10);
    //     $scope.jobs1 = [];
    //     // watch the user's jobs index for add events
    //     indexRef.on('child_added', function(indexSnap) {
    //          // fetch the job and put it into our list
    //          var jobListingId = indexSnap.key;
    //           console.log( `########-`);
    //          $scope.jobs1.push($scope.jobListings[jobListingId])
    //           console.log( `########-`);
    //          FirebaseRef.jobListings().child(jobListingId).on('value', function(snap) {
    //               // // trigger $digest/$apply so Angular syncs the DOM
    //               // $timeout(function() {
    //               //      if( snap.val() === null ) {
    //               //           // the job was deleted
    //               //           delete $scope.jobListings[jobListingId];
    //               //      }
    //               //      else {
    //               //           $scope.jobListings[jobListingId] = snap.val();
    //               //           console.log( `${jobListingId}idstiff------`);
    //               //      }
    //               // });
    //          });
    //     });
    //
    //     // watch the index for remove events
    //     indexRef.on('child_removed', function(snap) {
    //          // trigger $digest/$apply so Angular updates the DOM
    //          $timeout(function() {
    //               delete $scope.jobListings[snap.key];
    //          });
    //     });
    //
    //     console.log( `boom-----`);
    // }



    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

    $scope.isInProgress = function(job) {
      return false;
    }

    $scope.isUpcoming = function(job) {
      return false;
    }

    $scope.isPast = function(job) {
      return false;
    }

    // TODO Filter available jobs by search radius, etc
    // $scope.availableJobs = $firebaseArray(FirebaseRef.jobListings().orderByChild('hasEnoughPandas').equalTo(false));
    // $scope.availableJobs = $firebaseArray(JobsService.getOpenJobs());
    // console.log( "BLOOP")


    $scope.user = ProfileService.getUser();
    if( !$scope.user ) {
      $scope.pandaActive = false;
      console.log( '-----------------------' );
      console.log( `Unable to get user for ${user}` );
      console.log( '-----------------------' );
    } else {
      if( $scope.user.$id ) {
        //Allow the jobs list to load for inactive pandas, we filter it out of the GUI later
        // $scope.availableJobs = JobsService.getOpenJobs();
      }
    }
  }
})();
