(function() {
    'use strict';

    angular
    .module('app.panda')
    .controller('PandaJobDetailCtrl', PandaJobDetailCtrl);

    PandaJobDetailCtrl.$inject = [
      '$scope',
      '$rootScope',
      '$state',
      '$stateParams',
      'currentAuth',
      'FirebaseRef',
      'JobsService',
      '$ionicHistory',
      '$ionicPopup',
      'NotificationsService',
      '$http',
      'CacheService',
      'ModalService',
      'ProfileService'
    ];



    function PandaJobDetailCtrl($scope,$rootScope, $state, $stateParams, currentAuth, FirebaseRef, JobsService, $ionicHistory, $ionicPopup, NotificationsService, $http, CacheService, ModalService, ProfileService) {
      const DEFAULT_RATING = 5;
      const serverURL = CacheService.getServerURL();
      const pauseJobURL = serverURL + 'job/pause';
      // const accountSid = 'AC574d3ee3b1626061246da3ac267cc8d6';
      // const authToken = '4c8e67aa4f051fd65d8871116ad24300';
      //
      //
      // const test = require('twilio')(accountSid, authToken);
      const resumeJobURL = serverURL + 'job/resume';
      const addRatingURL = serverURL + 'addRatingForUser';
      var jobRef = null;

      console.log("Chore DETAIL CONTROLLER LOADED!!");
        var totalHours, remainingHours, id;
        JobsService.getJob($stateParams.id).$loaded(function(res) {
            $scope.job = res;
            totalHours = $scope.job.totalHours * 3600000;
            remainingHours = totalHours;
            $scope.jobStarted  = res.started;
            $scope.timerToggleLabel = ( !$scope.paused ) ? 'Pause Job' : 'Resume Job';

            setConsumerImage();

            console.log(`-------STATE ${$stateParams.state}`)
          if( $stateParams.state === NotificationsService.jobDetailState.RATE_USER ) {
            $scope.showRatingModal();
            console.log( 'SHOW THE RATING SCREEN');
          }
        });

        $scope.progress = 0;
        $scope.pendingRating = DEFAULT_RATING;

        $scope.call = function() {
          console.log("call clicked panda");
          if ($scope.job.pandasAccepted) {
            const pandaId = Object.keys($scope.job.pandasAccepted)[0];

            // FirebaseRef.profiles.child(pandaId).on('value', function(snapshot) {
            //   console.log("heros phone number(from)", snapshot.val().mobilePhone);
            //   console.log("this consumer number(to)", $scope.consumer.mobilePhone);
            //
            //    if (snapshot.val() && $scope.consumer) {
            //      console.log("heee");
                // $http.get('https://us-central1-purple-panda-ad119.cloudfunctions.net/call?to=1'+snapshot.val().mobilePhone+'&from=1' + $scope.consumer.mobilePhone).then(res => {
          //         if (res.status === 200) {
          //           $ionicPopup.alert({
          //             cssClass: 'my-custom-popup',
          //             title: 'Call Status',
          //             template: 'Call succeeded!',
          //             cssClass: 'my-custom-popup',
          //           });
          //         } else {
          //           $ionicPopup.alert({
          //             cssClass: 'my-custom-popup',
          //             title: 'Call Status',
          //             template: 'Call failed!',
          //             cssClass: 'my-custom-popup',
          //           })
          //         }
                // });
              // }
            // });
          }
        }




        $scope.isPast = function(job) {
          const endMoment = moment(job.endTime);
          return moment().isAfter(endMoment);
        }

        $scope.requestToStartJob = function() {
          let deltaToStart = moment($scope.job.startTime).diff(moment());
          if( deltaToStart > 0 && deltaToStart > 600000 )
          {
            showTooEarlyAlert();
            return;
          }

          $scope.job.requestToStart = true;
          $scope.job.$save();

          jobRef = FirebaseRef.jobListings().child($stateParams.id).on( 'value', jobUpdated );

          NotificationsService.sendNotificationToConsumer($scope.job, 'Start Request', 'The worker has requested to start the chore.',
            NotificationsService.jobDetailState.REQUEST_TO_START);
        };

        $scope.toggleJob = function() {
          if ($scope.timerToggleLabel === 'Pause Job') {
            console.log("PAUSING");
            $http.post(pauseJobURL, { jobId:$scope.job.$id} )
              .success(function() {})
              .error(
                function(error) {
                  console.log(`Error pausing job - ${error}`);
                });

            $scope.timerToggleLabel = 'Resume Job';
          } else {
            console.log("RESUMING");
            $http.post(resumeJobURL, { jobId:$scope.job.$id} )
              .success(function() {})
              .error(
                function(error) {
                  console.log(`Error resuming job - ${error}`);
                });

            $scope.timerToggleLabel = 'Pause Job';
          }

        }

        $scope.goToChat = function() {
            FirebaseRef.jobChatRooms.child($stateParams.id).orderByChild('pandaId').equalTo(currentAuth.uid).on('value', function(snapshot) {
                var chatRoomObj = snapshot.val();
                var roomIds = Object.keys(chatRoomObj);
                $state.go('app.panda.chat-room', { jobId: $stateParams.id, roomId: roomIds[0] });
            });
        }

        // TODO
        $scope.completeJob = function() {
          console.log("job is complete");

          //  return;
        };

      function showTooEarlyAlert() {
          var alertPopup = $ionicPopup.alert({
            cssClass: 'my-custom-popup',
            title: 'Sorry, Too Early',
            template: 'You cannot request to start a chore until 10 minutes before the scheduled start.',
            cssClass: 'my-custom-popup',
          });
          alertPopup.then(function(res) {});
        }

      function setConsumerImage() {
        if( !$scope.job || !$scope.job.createdBy ) {
          return;
        }

        //fetch the profile of the user who posted the job
        let consumer = $scope.job.createdBy;
        ProfileService.fetchProfile(consumer).$loaded(function(profile) {
          //userProfile is the field expected by the rating modal
          $scope.consumer = $scope.userProfile = profile;
        });
      }


      // $scope.goHome = function() {
      //     $ionicHistory.nextViewOptions({
      //       disableBack: true
      //     });
      //     $state.go('app.panda.jobs-list');
      //   };

       $scope.showConfirmCancel = function() {
         var confirmPopup = $ionicPopup.confirm({
           cssClass: 'my-custom-popup',
           title: 'Cancel Job',
           template: 'Are you sure you want to cancel this chore?',
           cancelText: 'No',
           okText: 'Yes',
           cancelType: 'button-assertive',
           okType: 'button-assertive',
          // okType: 'button-positive',

         });

         confirmPopup.then(function(res) {
           if(res) {
             $scope.cancelJob();
             // $scope.goHome();
           } else {
           }
         });
       };

        $scope.goHome = function() {
            $ionicHistory.nextViewOptions({
              disableBack: true,
              // historyRoot: true
            });
            $state.go('app.panda.jobs-list');
          };

           $scope.showConfirmCancel = function() {
             var confirmPopup = $ionicPopup.confirm({
               cssClass: 'my-custom-popup',
               title: 'Cancel Job',
               template: 'Are you sure you want to cancel this chore?',
               cancelText: 'No',
               okText: 'Yes',
               cancelType: 'button-assertive',
               okType: 'button-assertive',
              // okType: 'button-positive',

             });

             confirmPopup.then(function(res) {
               if(res) {
                 $scope.cancelJob();
                 $scope.goHome();
               } else {
               }
             });
           };

           $scope.showNotes = function() {
             var alertPopup = $ionicPopup.alert({
               title: 'Notes',
               cssClass: 'my-custom-popup2',
               okText: 'Close',
               template: $scope.job.notes
             });
             alertPopup.then(function() {});
           };


      $scope.cancelJob = function(user) {

        FirebaseRef.jobListings().child($scope.job.$id).once('value').then( (snapshot) => {
        // FirebaseRef.jobListings().child($scope.job.$id + '/pandasAccepted/').once('value').then( (snapshot) => {
          let job = snapshot.val();
          let pandas = job.pandasAccepted;

          if( !pandas ) { pandas = {}; }
          delete pandas[currentAuth.uid];
          const updates = { 'pandaCount': Object.keys(pandas).length,
            'hasEnoughPandas': false,
            'pandasAccepted' : pandas };
          FirebaseRef.jobListings().child($scope.job.$id).update(updates);
          JobsService.updateJobHistory($scope.job.$id, job);
        });

        // $scope.job.hasEnoughPandas = false;
        // $scope.job.pandaCount -= 1;
        // $scope.job.$save();
        // FirebaseRef.jobListings().child($scope.job.$id + '/pandasAccepted/' + currentAuth.uid).remove();

        FirebaseRef.profiles.child(currentAuth.uid + '/jobListings/' + $scope.job.$id).remove();

        NotificationsService.sendNotificationToConsumer(
          $scope.job,
          'Message from Chore',
          'Your Chore Hero has canceled working on your chore. You will be notified when another worker is assigned. -Chore Team',
          NotificationsService.jobDetailState.SHOW_JOB);

        // $ionicHistory.clearCache();
        $ionicHistory.nextViewOptions({
          // disableBack: true
          historyRoot: true
        });

        $rootScope.$broadcast('jobHistoryDeleted', {id:$scope.job.$id});

        $state.go('app.panda.jobs-list');
      };


      //Duplicate code with consumer job detail :-(
      $scope.showRatingModal = () => {
        $scope.applicationDetail = $scope;

        ModalService
          .applicationDetail('app/core/rate-user-modal.html', $scope)
          .then(function(modal) {
            $scope.currentModal = modal;
            modal.show();
          });
      };

      $scope.onRatingsClick = (event) => {
        $scope.pendingRating = event.rating;
      };

      $scope.closeRatingModal = () => {
        if( $scope.currentModal ) {
          ModalService.userPressedOK($scope.currentModal);
          sendNewRatingToServer();
        }
      };

      function sendNewRatingToServer() {
        $http.post(addRatingURL, { jobId:$scope.job.$id,
          userId:$scope.consumer.$id,
          rating:$scope.pendingRating} )
          .success(function() {
            console.log( `New rating added to user ${$scope.consumer.firstName} ${$scope.consumer.lastName} for job ${$scope.job.$id}` );
          })
          .error(
            function(error) {
              console.log(`Error starting job - ${error}`);
            });
      }

      //This is used to signal the panda(s) that the job has started
      function jobUpdated(snapshot) {
        let val = snapshot.val();

        console.log( `Update received for ${val.$id}`);
        $scope.jobStarted  = val.started;
        $scope.timerStatus = ( $scope.jobStarted ) ? 'Pause Job' : 'Resume Job';
        if(val.completed) {
          FirebaseRef.jobListings().off( 'value', jobUpdated );
        }
      }
    }
})();
