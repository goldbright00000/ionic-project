(function() {
    'use strict';

    angular
    .module('app.panda')
    .controller('JobApplyCtrl', JobApplyCtrl);

    JobApplyCtrl.$inject = [
      '$scope',
      '$state',
      '$stateParams',
      '$http',
      'Auth',
      'currentAuth',
      'FirebaseRef',
      'JobsService',
      '$ionicModal',
      '$cordovaDatePicker',
      '$ionicHistory',
      'NotificationsService',
      'CacheService',
      'ProfileService',
      '$ionicPopup'
    ];

    function JobApplyCtrl($scope, $state, $stateParams, $http, Auth, currentAuth, FirebaseRef, JobsService, $ionicModal, $cordovaDatePicker, $ionicHistory, NotificationsService, CacheService, ProfileService, $ionicPopup) {

        var now;
        $scope.now = new Date();
        var options = {
            date: now,
            mode: 'datetime',
            minDate: now,
            allowOldDates: false,
            allowFutureDates: true,
            doneButtonLabel: 'DONE',
            doneButtonColor: '#000000',
            cancelButtonLabel: 'CANCEL',
            cancelButtonColor: '#000000'
        };

        $scope.goList = function() {
          $state.go( 'app.panda.jobs-list' );
        };

        $ionicModal.fromTemplateUrl('app/panda/job-apply/confirm-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.confirmModal = modal;

        });

        $ionicModal.fromTemplateUrl('app/panda/job-apply/decline-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.declineModal = modal;
        });
        // $scope.$on('$destroy', function() {
        //     console.log('modal DESTROYED');
        // });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function(event, modal) {
            $ionicHistory.nextViewOptions({
                historyRoot: true
            });
            if(modal.modalEl.id === 'confirm-modal') {
                $state.go('app.panda.job-detail',
                  {
                    id: $scope.job.$id,
                    state: NotificationsService.jobDetailState.SHOW_JOB
                  });
            } else if(modal.modalEl.id === 'decline-modal') {
                $state.go('app.panda.jobs-list');
            }
        });

        // Execute action on remove modal
        $scope.$on('modal.removed', function() {
            // Execute action
            console.log('modal REMOVED');
        });

        JobsService.getJob($stateParams.id).$loaded(function(res) {
            $scope.job = res;
            $scope.filteredAddress = createFilteredAddress( $scope.job );
            $scope.jobRequest = {
                  // yourHours: parseFloat(res.totalHours / res.pandasNeeded),
                  yourHours: res.totalHours,
                  checkInTime: now
            };

            if(res.endTime) {
                options.maxDate = moment(res.endTime).subtract($scope.jobRequest.yourHours, 'hours');
            }
        });

        JobsService.getJobPandas($stateParams.id).$loaded(function(res) {
            $scope.pandasAccepted = res; //pandas that have accepted the job
        });

        $scope.showDatePicker = function() {
            $cordovaDatePicker.show(options).then(function(date) {
                if(date) {
                    $scope.job.startTime = date;
                    console.log('Changed the start date to ' + date);
                }
                else {
                    console.log('Date undefined'); // Cancel clicked
                }
            });
        };

        $scope.showNotes = function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Notes',
            cssClass: 'my-custom-popup2',
            okText: 'Close',
            template: $scope.job.notes
          });
          alertPopup.then(function() {});

        };

        $scope.requestJob = function() {
            // IF the job has fewer pandas than needed
            if($scope.pandasAccepted && $scope.pandasAccepted.length < $scope.job.pandasNeeded) {
              assignJobIfNoOverlappingJobs();
            }
            else { //job is already full
                $scope.declineModal.show().then(function() {
                  console.log('modal up');
                }).catch(function(){
                  console.log('no modal');
                });
            }
        };

        function createFilteredAddress( job ) {
          let street ='', city = '', zip = '';

          job.location.address_components.forEach( (component) => {
            if(component.types.indexOf('route') > -1) {
              street = component.long_name;
            }
            if(component.types.indexOf('locality') > -1) {
              city = component.long_name;
            }
            if(component.types.indexOf('postal_code') > -1) {
              zip = component.long_name;
            }
          });

          return `${street} ${city}, SC ${zip}`;
        }

        function assignJobIfNoOverlappingJobs() {
          //lengthen the job being requested by 30 minutes on either end
          const targetJobStart = moment($scope.job.startTime).subtract( 30, 'minutes');
          const targetJobEnd = moment($scope.job.endTime).add( 30, 'minutes');

          $scope.profile = ProfileService.fetchProfile(currentAuth.uid)
            .$loaded().then( (profile) => {
              let promises = [];
              if( profile.jobListings ) {
                for( let i=0; i<Object.keys(profile.jobListings).length; i++ ) {
                  const jobId = Object.keys(profile.jobListings)[i];
                      promises.push(FirebaseRef.jobListings().child(jobId).once('value').then( (snapshot) => {
                      const job = snapshot.val();
                      if( !job || job.completed || job.cancelled ) {
                        return false;
                      }
                      const jobStart = moment( job.startTime );
                      const jobEnd = moment( job.endTime );
                      if( jobStart.isBetween(targetJobStart, targetJobEnd) ) {
                        return true;
                      }
                      if( jobEnd.isBetween(targetJobStart, targetJobEnd) ) {
                        return true;
                      }
                      if( targetJobStart.isBetween( jobStart, jobEnd ) &&
                          targetJobStart.isBetween( jobStart, jobEnd ) ) {
                        return true;
                      }
                      return false;
                    }));
                }
                Promise.all(promises).then( (values) => {
                  // values.forEach( val => { console.log('Promise -> '  + val)});
                  if( !values.reduce( (result,init) => { return result || init; }, false ) ) {
                    assignPandaToJob();
                  } else {
                    showConflictPopup();
                  }
                });
              } else {
                assignPandaToJob();
              }
            });


          // //TODO see if we can share the list with the menu stuff so another refetch isn't required
          // var profileRef = FirebaseRef.profiles.child(currentAuth.uid);
          //
          // profileRef.once('child_added', function(snapshot) {
          //     const job = snapshot.val();
          //     GET THE QUERY TO RETURN A LIST
          //   });
        }

        function assignPandaToJob() {
            // add Choreid to job's accepted pandas array
            const hasEnoughPandas = ($scope.pandasAccepted && $scope.pandasAccepted.length + 1 === $scope.job.pandasNeeded);

            FirebaseRef.jobListings().child($scope.job.$id + '/pandasAccepted/').once('value').then( (snapshot) => {
              let pandas = snapshot.val();
              if( !pandas ) { pandas = {}; }
              pandas[currentAuth.uid] = true;
              const updates = { 'pandaCount': Object.keys(pandas).length,
                'hasEnoughPandas': hasEnoughPandas,
                'pandasAccepted' : pandas };
              FirebaseRef.jobListings().child($scope.job.$id).update(updates);
            });

            FirebaseRef.profiles.child(currentAuth.uid + '/jobListings/' + $scope.job.$id).set(true);

            const now = new Date();
            const createdAt = now.getTime();

            const newChatRoom = {
                createdAt: createdAt,
                consumerId: $scope.job.createdBy,
                pandaId: currentAuth.uid
            };

            FirebaseRef.jobChatRooms.child($scope.job.$id).push(newChatRoom);

            $scope.confirmModal.show().then(function() {
                console.log('modal up');
            }).catch(function(){
                console.log('no modal');
            });

          NotificationsService.sendNotificationToConsumer($scope.job, 'Worker Assigned', 'A Chore Hero has signed up for your Chore! You can message them directly if you have any further information for them.',
            NotificationsService.jobDetailState.SHOW_JOB);
        }

      function showConflictPopup() {
        var alertPopup = $ionicPopup.alert({
          title: 'Time conflict',
          template: 'Sorry, this job overlaps with a chore you\'ve already signed up for.',
          cssClass: 'my-custom-popup',
        });
        alertPopup.then(function(res) {
          $ionicHistory.goBack();
        });
      }

    }
})();
