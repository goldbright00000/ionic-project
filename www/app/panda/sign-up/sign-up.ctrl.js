(function() {
  'use strict';

  angular
    .module('app.panda')
    .controller('PandaSignUpController', PandaSignUpController);

  PandaSignUpController.$inject = [
    '$scope',
    '$state',
    'Auth',
    '$ionicHistory',
    '$ionicPush',
    'ProfileService',
    'StripeCharge',
    'FirebaseRef',
    '$ionicPlatform',
    '$cordovaImagePicker',
    '$cordovaFile'
  ];

  function PandaSignUpController($scope, $state, Auth, $ionicHistory, $ionicPush, ProfileService, StripeCharge, FirebaseRef, $ionicPlatform, $cordovaImagePicker, $cordovaFile) {

    //
    //       $scope.DynamicStripeAuthorizeUrl = STRIPE_URL_AUTHORIZE + "?userId=" + AuthData.uid
    // + "&token=" + fbAuthToken;
    window.Intercom("boot", {
      app_id: "rus6c6vh",
      custom_launcher_selector: '#my_custom_link'
    });
    // VARIABLES
    $scope.user = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
      paypal: '',
      streetAddress: '',
      city: '',
      state: '',
      zip: '',
      mobilePhone: '',
      idPhoto: ''
    };

    $scope.userStripe = {
      name: '',
      address: '',
      dob: '',
    };

    resetErrors();

    $scope.register = function(form) {
      resetErrors();
      console.log('Signing up user...');

      if (form.$valid) {

        // Create a new user
        Auth.register($scope.user)
          .then(function(data) {
            $scope.message = "User created with uid: " + data.user.uid;
            // User successfully created
            console.log("Created user: " + data.user.uid);
            console.log("Adding user profile to db");

            // set new registered user's profile info in services
            ProfileService.setUser(data.user);

            // Add profile to Users db
            var profilesRef = firebase.database().ref().child('profiles');
            profilesRef.child(data.user.uid).set({
              firstName: $scope.user.firstName,
              lastName: $scope.user.lastName,
              email: $scope.user.email,
              paypal: $scope.user.paypal,
              streetAddress: $scope.user.streetAddress,
              city: $scope.user.city,
              state: $scope.user.state,
              zip: $scope.user.zip,
              mobilePhone: $scope.user.mobilePhone,
              idPhoto: $scope.user.idPhoto,
              active: false,
              role: 'panda',
              avatar: null,
              ratings:{'000000': {'rating':'5'}}
            });

            localStorage.setItem("ppuid", data.user.uid);
            localStorage.setItem("role", 'panda');

            // Added profile to db, redirect to assessment
            console.log("Added profile to db, going to assessment");

            $ionicHistory.nextViewOptions({
              historyRoot: true
            });
            $state.go('app.panda.onboarding-1');
          }).catch(function(error) {
            // On Auth error, set error message
            setError(error.code);
            console.log('Error creating user: ' + error.code + ' - ' + error.message);
          });
      } else {

        // invalid phone
        if ( !$scope.user.mobilePhone) {
          setError( 'custom/invalid-phone');
        }
        // send default error message
        setError( '' );
        console.log('invalid form');
      }
    };

    $scope.onboarding2 = function() {
      $state.go('app.panda.onboarding-2');
    };
    $scope.onboarding3 = function() {
      $state.go('app.panda.onboarding-3');
    };
    $scope.onboardingExit = function() {
      $state.go('app.panda.profile-picture');
    };
    $scope.resetPassword = function() {
      $state.go('app.panda.password-reset');
    };
    $scope.terms = function() {
      $state.go('app.panda.terms');
    };
    $scope.privacy = function() {
      $state.go('app.panda.privacy');
    };
    $scope.editProfile = function() {
      $state.go('app.panda.edit-profile');
    };
    $scope.updateProfile = function() {
      $scope.profile.$save()
    };
    $scope.addCard = function() {
      $state.go('app.panda.add-card');
    };

    document.addEventListener("deviceready", function() {
      var fileName;
      var path;
      console.log('--------- '  + cordova.file);

      // Got the core of this functionality from aaronksaunders
      // https://github.com/aaronksaunders/firebaseStorageApp
      $scope.pickImage = function () {
        var pickerArgs = {
            'selectMode': 100, //101=picker image and video , 100=image , 102=video
            'maxSelectCount': 1, //default 40 (Optional)
            'maxSelectSize': 188743680, //188743680=180M (Optional) only android
        };

        MediaPicker.getMedias(pickerArgs, function(medias) {
            //medias [{mediaType: "image", path:'/storage/emulated/0/DCIM/Camera/2017.jpg', uri:"android retrun uri,ios retrun URL" size: 21993}]
            getThumbnail(medias);

          }, function(e) { console.log('Error in getMedias');
        });
      };

      function getThumbnail(medias) {
        for (var i = 0; i < medias.length; i++) {
          console.log('IN PANDA THUMBNAIL');
            medias[i].thumbnailQuality=50;
            //loadingUI(); //show loading ui
            MediaPicker.extractThumbnail(medias[i], function(data) {
                // saveToFirebase(data);
                console.log('=======================');
                Object.keys(data).forEach(key => console.log(key));
                console.log('=======================');

                const imageData = 'data:image/jpeg;base64,' + data.thumbnailBase64;
                saveToFirebase(imageData);
            }, function(e) { console.log(e) });
        }
      }

      function saveToFirebase(file) {
        let uid = localStorage.getItem("ppuid" );
        if( !uid ) {
          console.log( 'Error: Unable to retrieve uid for panda.');
          return;
        }
        var userProfileRef = FirebaseRef.profiles.child(uid);

        return userProfileRef.child('profilePicture').set(file);
      }
    });

    // Clear error message in DOM
    function resetErrors() {
      $scope.error = {
        email: '',
        phone: '',
        password: '',
        message: ''
      };
    }

    // Set error message in DOM depending on Firebase error code
    function setError(errorCode) {
      switch (errorCode) {
        case 'auth/invalid-email':
          $scope.error.email = true;
          $scope.error.message = 'Invalid email';
          break;
        case 'auth/email-already-in-use':
          $scope.error.email = true;
          $scope.error.message = 'Email already in use';
          break;
        case 'auth/weak-password':
          $scope.error.password = true;
          $scope.error.message = 'Password not strong enough';
          break;
        case 'custom/invalid-phone':
          $scope.error.message = 'Phone number not in correct form. Must be in form xxx-xxx-xxxx';
          break;
        default:
          $scope.error.message = 'There was an error signing you up.';
      }
    }

    //this code is unused but cool
    function encryptSSN(plainTextSSN) {
      const secret = StripeCharge.getCryptoSecret();
      const ciphertext = CryptoJS.AES.encrypt(plainTextSSN, secret).toString();

      return ciphertext;
    }
  }
})();
