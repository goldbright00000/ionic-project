(function() {
  'use strict';

  angular
  // .module('app.panda', [ "ngOnload" ])
    .module('app.panda')
    .controller('AddCardController', AddCardController);

  AddCardController.$inject = [
    '$scope',
    '$state',
    'Auth',
    '$ionicHistory',
    '$ionicPush',
    'ProfileService'
    ];

  function AddCardController($scope, $state, Auth, $ionicHistory, $ionicPush, ProfileService) {
    $scope.doCheckout = function(token) {
      console.log( "====" + token );
      alert("Got Stripe token: " + token.id);
    };
  }
})();
