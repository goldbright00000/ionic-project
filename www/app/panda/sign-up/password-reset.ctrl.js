/*jshint esversion: 6 */

(function() {
  'use strict';

  angular
    .module('app.panda')
    .controller('PasswordResetCtrl', PasswordResetCtrl);

  PasswordResetCtrl.$inject = [
    '$scope',
    '$state',
    'Auth',
    'FirebaseRef',
    '$cordovaImagePicker',
    '$cordovaFile',
    '$ionicHistory',
    '$ionicPush',
    '$ionicPopup'
  ];

  function PasswordResetCtrl($scope, $state, Auth, FirebaseRef, $cordovaImagePicker, $cordovaFile, $ionicHistory, $ionicPush, $ionicPopup) {
    $scope.vars = {};
    resetData();
    resetErrors();

    $scope.changePassword = function() {
      resetErrors();

      if( $scope.vars.newPassword === $scope.vars.confirmPassword ) {
        var user = firebase.auth().currentUser;
        if( !user ) {
          console.log( 'User object not set' );
          return;
        }
        let newPassword = $scope.vars.newPassword;
        let currentPassword = $scope.vars.currentPassword;

        var cred = firebase.auth.EmailAuthProvider.credential(
            user.email, currentPassword);
        user.reauthenticateWithCredential(cred)
        .then( function() {
          user.updatePassword(newPassword)
          .then(function() {
            resetData();
            showPopup();
          }).catch(function(error) {
            setError(error)
          });
        })
        .catch(function(error) {
          //"auth/requires-recent-login"
          console.log( "Error reauthenticating credential" + error);
          setError(error);
        });

      } else {
        setError({ code:'local/passwordsDontMatch', message:"Passwords do not match"});
      }
      // console.log($scope.vars);
    };

    // Clear error message in DOM
    function resetErrors() {
      $scope.error = {
        message: ''
      };
    }

    function resetData() {
      $scope.vars.currentPassword = '';
      $scope.vars.newPassword = '';
      $scope.vars.confirmPassword = '';
    }

    function showPopup() {
        var alertPopup = $ionicPopup.alert({
          title: 'Success',
          template: 'Password changed successfully.',
          cssClass: 'my-custom-popup',
        });
        alertPopup.then(function(res) {
          $ionicHistory.goBack();
        });
      }

    // Set error message in DOM depending on Firebase error code
    function setError(error) {
      switch (error.code) {
        case 'auth/wrong-password':
          $scope.error.password = true;
          $scope.error.message = 'Current password is incorrect';
          break;
        case 'local/passwordsDontMatch':
          $scope.error.password = true;
          $scope.error.message = 'Passwords don\'t match';
          break;
        case 'auth/weak-password':
          $scope.error.password = true;
          $scope.error.message = 'Password must be at least 6 characters';
          break;
        case 'auth/too-many-requests':
          $scope.error.password = true;
          $scope.error.message = 'Too many request. Try again later.';
          break;
        default:
          $scope.error.message = 'There was an error resetting your password.';
      }

      $scope.$apply(function() {      });

    }

  }
})();
