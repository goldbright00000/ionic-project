(function() {
  'use strict';

  angular
    .module('app.panda')
    .controller('StripeController', StripeController);

  StripeController.$inject = [
    '$scope',
    '$state',
    'Auth',
    '$ionicHistory',
    '$ionicPush',
    'StripeService',
    '$location',
  ];

  function StripeController($scope, $state, Auth, $ionicHistory, $ionicPush, StripeService, $location) {
    var AuthData = firebase.auth();
    console.log(AuthData.currentUser.uid);
    var STRIPE_URL_AUTHORIZE = 'http://192.168.0.3:9311/' + "/authorize";

    $scope.status = {
      loadingAuthToken: false
    };
    loadWhenEnteringView();

    function loadWhenEnteringView() {
      console.log("you loading?");
      $scope.status["loadingAuthToken"] = true;
      StripeService.generateFBAuthToken(AuthData.currentUser.uid).then(
        function(fbAuthToken) {
          //  update the dynamic url for stripe connect
          $scope.DynamicStripeAuthorizeUrl = STRIPE_URL_AUTHORIZE +
            "?userId=" + AuthData.currentUser.uid + "&token=" + fbAuthToken;
          console.log($scope.DynamicStripeAuthorizeUrl);
          $scope.status["loadingAuthToken"] = false;
        },
        function(error) {

          console.log(error);
        });

    };

    $scope.stripeConnect = function() {
      var ref = window.open($scope.DynamicStripeAuthorizeUrl, '_blank', 'location=no')
      ref.addEventListener('loadstart', function(event) {
        console.log("url", event.url);
        if (event.url.includes("callback?scope=read_write&code")) {
          console.log("RUNNING EVENT!");
          ref.close();
          if ($location.$$url === "/app/consumer/stripe") {
            $state.go('app.consumer.create-job.confirm')
          } else {
            $state.go('app.panda.jobs-list');
          }
        } else {
          if ($location.$$url === "/app/consumer/stripe") {
            $state.go('app.consumer.stripe')
          } else {
            $state.go('app.panda.stripe');
          }
        };

    })
  }

    $scope.onboarding2 = function() {
      $state.go('app.panda.onboarding-2');
    }
    $scope.onboarding3 = function() {
      $state.go('app.panda.onboarding-3');
    }
    $scope.onboardingExit = function() {
      $state.go('app.panda.jobs-list');
    }
    $scope.resetPassword = function() {
      $state.go('app.panda.password-reset');
    }
    $scope.terms = function() {
      $state.go('app.panda.terms');
    }
    $scope.privacy = function() {
      $state.go('app.panda.privacy');
    }
    $scope.editProfile = function() {
      $state.go('app.panda.edit-profile');
    }
    $scope.updateProfile = function() {
      $scope.profile.$save()
    }

    // Clear error message in DOM
    function resetErrors() {
      $scope.error = {
        email: '',
        phone: '',
        password: '',
        message: ''
      };
    }

    // Set error message in DOM depending on Firebase error code
    function setError(errorCode) {
      switch (errorCode) {
        case 'auth/invalid-email':
          $scope.error.email = true;
          $scope.error.message = 'Invalid email';
          break;
        case 'auth/email-already-in-use':
          $scope.error.email = true;
          $scope.error.message = 'Email already in use';
          break;
        case 'auth/weak-password':
          $scope.error.password = true;
          $scope.error.message = 'Password not strong enough';
          break;
        default:
          $scope.error.message = 'There was an error signing you up.';
      }
    }
  }
})();
