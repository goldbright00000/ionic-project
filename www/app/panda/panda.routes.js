(function() {
    'use strict';

    angular
    .module('app.panda')
    .config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {
        $stateProvider
            .state('app.panda', {
                url: '/panda',
                abstract: true,
                views: {
                  'menuContent': {
                    controller: function() {
                      console.log('In Choremodule controller');
                    }
                  }
                }
            })
            .state('app.panda.sign-up', {
                url: '/sign-up',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/sign-up.html',
                        controller: 'PandaSignUpController'
                    }
                }
            })
            .state('app.panda.profile-picture', {
                url: '/profile-picture',
                views: {
                    'menuContent@app': {
                        controller: 'ConsumerProfilePictureCtrl',
                        templateUrl: 'app/panda/sign-up/profile-picture.html'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.panda.stripe', {
                url: '/stripe',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/stripe.html',
                        controller: 'StripeController'
                    }
                }
            })
            .state('app.panda.onboarding-1', {
                url: '/onboarding-1',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/onboarding-1.html',
                        controller: 'PandaSignUpController'
                    }
                }
            })
            .state('app.panda.onboarding-2', {
                url: '/onboarding-2',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/onboarding-2.html',
                        controller: 'PandaSignUpController'
                    }
                }
            })
            .state('app.panda.onboarding-3', {
                url: '/onboarding-3',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/onboarding-3.html',
                        controller: 'PandaSignUpController'
                    }
                }
            })
            .state('app.panda.settings', {
                url: '/settings',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/settings.html',
                        controller: 'PandaSignUpController'
                    }
                },
              resolve: {
                'currentAuth': ['Auth', function(Auth) {
                  return Auth.requireSignIn();
                }]
              }
            })
            .state('app.panda.password-reset', {
                url: '/password-reset',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/password-reset.html',
                        controller: 'PasswordResetCtrl'
                    }
                }
            })
            .state('app.panda.edit-profile', {
                url: '/edit-profile',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/edit-profile.html',
                        controller: 'PandaSignUpController'
                    }
                },
                resolve: {
                  'currentAuth': ['Auth', function(Auth) {
                    return Auth.requireSignIn();
                  }]
                }
            })
            .state('app.panda.add-card', {
                url: '/add-card',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/add-card.html',
                        controller: 'AddCardController'
                    }
                }
            })
            .state('app.panda.jobs-list', {
                url: '/jobs',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/jobs-list/jobs-list.html',
                        controller: 'JobsListCtrl'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })




            .state('app.panda.yourJobs', {
                url: '/yourJobs',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/yourJobs/yourJobs.html',
                        controller: 'yourJobsCtrl'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })


            .state('app.panda.jobs-history', {
                url: '/jobs-history',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/jobs-history/jobs-history.html',
                        controller: 'PandaJobsHistoryCtrl'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })







            .state('app.panda.job-apply', {
                url: '/job/:id',
                abstract: true,
                views: {
                  'menuContent@app': {
                    controller: 'JobApplyCtrl',
                    template: '<ion-nav-view name="jobApply"></ion-nav-view>'
                  }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.panda.job-apply.confirm', {
                url: '',
                views: {
                  'jobApply': {
                        templateUrl: 'app/panda/job-apply/job-confirm.html'
                  }
                }
            })
            .state('app.panda.job-apply.time', {
                url: '/time',
                views: {
                  'jobApply': {
                        templateUrl: 'app/panda/job-apply/job-time.html'
                  }
                }
            })
            .state('app.panda.job-detail', {
                url: '/job-detail/:id/:state',
                views: {
                  'menuContent@app': {
                    controller: 'PandaJobDetailCtrl',
                    templateUrl: 'app/panda/job-detail/job-detail.html'
                  }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.panda.chat-room', {
                url: '/chat/:jobId/:roomId',
                views: {
                  'menuContent@app': {
                    controller: 'PandaChatDetailCtrl',
                    templateUrl: 'app/panda/chat/chat-room.html'
                  }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            });
    }
})();
