(function() {
    'use strict';

    angular
    .module('app.panda')
    .controller('PandaChatDetailCtrl', PandaChatDetailCtrl);

    PandaChatDetailCtrl.$inject = [
      '$scope',
      '$state',
      '$stateParams',
      '$ionicScrollDelegate',
      'currentAuth',
      'ChatService',
      'ProfileService',
      '$ionicHistory'
    ];

    function PandaChatDetailCtrl($scope, $state, $stateParams, $ionicScrollDelegate, currentAuth, ChatService, ProfileService, $ionicHistory) {
        ChatService.getChatRoom($stateParams.jobId, $stateParams.roomId).$loaded().then(function(res) {
            $scope.room = res;
            $scope.consumer = ProfileService.fetchProfile($scope.room.consumerId);
        });

        $scope.goBack = function() {
          $ionicHistory.goBack();
        }

        $scope.messages = ChatService.getChatMessages($stateParams.roomId);

        // Add advisor message to messages array
        $scope.submitMessage = function() {
            var now = Date.now();
            var newMessage = {
                author: currentAuth.uid,
                content: $scope.newMessageContent,
                createdAt: now
            };

            $scope.messages.$add(newMessage).then(function(ref) {
                $scope.newMessageContent = '';

                $scope.room.lastMessage = newMessage;
                $scope.room.$save().then(function(ref) {
                    console.log('Saved message as last room message');
                }, function(error) {
                    console.log("Error:", error);
                });
            });
        }

        $scope.$watch('messages', function(newValue, oldValue) {
            // $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollBottom(false);
        }, true);
    }
})();
