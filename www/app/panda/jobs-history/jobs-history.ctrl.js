(function() {
  'use strict';

  angular
    .module('app.panda')
    .controller('PandaJobsHistoryCtrl', PandaJobsHistoryCtrl);

  PandaJobsHistoryCtrl.$inject = [
    '$scope',
    '$state',
    'Auth',
    'currentAuth',
    'FirebaseRef',
    '$firebaseArray',
    '$ionicHistory',
    'JobsService',
    '$rootScope',
    'ProfileService',
    '$timeout'
  ];

  function PandaJobsHistoryCtrl($scope, $state, Auth, currentAuth, FirebaseRef, $firebaseArray, $ionicHistory, JobsService, $rootScope, ProfileService, $timeout) {
    $scope.$on('jobListUpdated', function(event,data) {
      console.log( 'update in panda history controller' );
      // refreshList();
    });

      window.Intercom('boot', {
        app_id: 'rus6c6vh',
        email: currentAuth.email, // Email address
        custom_launcher_selector: '#my_custom_link'
      });

      $scope.acceptedWorkersLength = function() {
        if( !$scope.job.pandasAccepted ) return 0;

        return Object.keys($scope.job.pandasAccepted).length;
      };

      $scope.isUpcoming = function(job) {
        const endMoment = moment(job.endTime);
        return moment().isBefore(endMoment);
      }

      $scope.isPast = function(job) {
        const endMoment = moment(job.endTime);
        return moment().isAfter(endMoment);
      }

      $scope.isBooked = function() {
        return $scope.job.pandasNeeded === $scope.acceptedWorkersLength();
      };

      function getUserJobs(user) {

          var indexRef = FirebaseRef.profiles.child(user.uid + '/jobListings').limitToLast(10);
          $scope.jobs1 = [];
          // watch the user's jobs index for add events
          indexRef.on('child_added', function(indexSnap) {
               // fetch the job and put it into our list
               var jobListingId = indexSnap.key;
                console.log( `########-`);
               $scope.jobs1.push($scope.jobListings[jobListingId])
                console.log( `########-`);
               FirebaseRef.jobListings().child(jobListingId).on('value', function(snap) {
                    // // trigger $digest/$apply so Angular syncs the DOM
                    // $timeout(function() {
                    //      if( snap.val() === null ) {
                    //           // the job was deleted
                    //           delete $scope.jobListings[jobListingId];
                    //      }
                    //      else {
                    //           $scope.jobListings[jobListingId] = snap.val();
                    //           console.log( `${jobListingId}idstiff------`);
                    //      }
                    // });
               });
          });

          // watch the index for remove events
          indexRef.on('child_removed', function(snap) {
               // trigger $digest/$apply so Angular updates the DOM
               $timeout(function() {
                    delete $scope.jobListings[snap.key];
               });
          });

          console.log( `boom-----`);
      }



      $scope.goBack = function() {
        $ionicHistory.goBack();
      };

      // TODO Filter available jobs by search radius, etc
      // $scope.availableJobs = $firebaseArray(FirebaseRef.jobListings().orderByChild('hasEnoughPandas').equalTo(false));
      // $scope.availableJobs = $firebaseArray(JobsService.getOpenJobs());
      // console.log( "BLOOP")


      $scope.user = ProfileService.getUser();
      if( !$scope.user ) {
        $scope.consumerActive = false;
        console.log( '-----------------------' );
        console.log( `Unable to get user for ${user}` );
        console.log( '-----------------------' );
      } else {
        if( $scope.user.$id ) {
          //Allow the jobs list to load for inactive pandas, we filter it out of the GUI later
          // $scope.availableJobs = JobsService.getOpenJobs();
        }
      }

      $rootScope.$on('jobHistoryDeleted', function(event, args) {
        console.log( 'broadcast received');

        $timeout(function() {
            delete $scope.jobListings[args.id];
        // $scope.$apply();
      });
    });

    $scope.jobListings = JobsService.getJobHistory();
  }
})();
