(function() {
  'use strict';

  angular
    .module('app.consumer')
    .controller('ConsumerJobsHistoryCtrl', ConsumerJobsHistoryCtrl);

  ConsumerJobsHistoryCtrl.$inject = [
    '$scope',
    '$state',
    'Auth',
    'currentAuth',
    'FirebaseRef',
    '$firebaseArray',
    '$ionicHistory',
    'JobsService',
    '$rootScope',
    'ProfileService'
  ];

  function ConsumerJobsHistoryCtrl($scope, $state, Auth, currentAuth, FirebaseRef, $firebaseArray, $ionicHistory, JobsService, $rootScope, ProfileService) {

    $scope.$on('jobListUpdated', function(event,data) {
      console.log( 'update in consumer history controller' );
      // refreshList();
    });

    window.Intercom('boot', {
      app_id: 'rus6c6vh',
      email: currentAuth.email, // Email address
      custom_launcher_selector: '#my_custom_link'
    });

    $scope.acceptedWorkersLength = function(job) {
      if( !job.pandasAccepted ) { return 0; }

      return Object.keys(job.pandasAccepted).length;
    };

    $scope.isUpcoming = function(job) {
      const endMoment = moment(job.endTime);
      return moment().isBefore(endMoment);
    }

    $scope.isPast = function(job) {
      const endMoment = moment(job.endTime);
      return moment().isAfter(endMoment);
    }

    $scope.isBooked = function(job) {
      return job.pandasNeeded === $scope.acceptedWorkersLength(job);
    };

    function getUserJobs(user) {

        var indexRef = FirebaseRef.profiles.child(user.uid + '/jobListings').limitToLast(10);
        $scope.jobs1 = [];
        // watch the user's jobs index for add events
        indexRef.on('child_added', function(indexSnap) {
             // fetch the job and put it into our list
             var jobListingId = indexSnap.key;
              console.log( `########-`);
             $scope.jobs1.push($scope.jobListings[jobListingId])
              console.log( `########-`);
             FirebaseRef.jobListings().child(jobListingId).on('value', function(snap) {
                  // // trigger $digest/$apply so Angular syncs the DOM
                  // $timeout(function() {
                  //      if( snap.val() === null ) {
                  //           // the job was deleted
                  //           delete $scope.jobListings[jobListingId];
                  //      }
                  //      else {
                  //           $scope.jobListings[jobListingId] = snap.val();
                  //           console.log( `${jobListingId}idstiff------`);
                  //      }
                  // });
             });
        });

        // watch the index for remove events
        indexRef.on('child_removed', function(snap) {
             // trigger $digest/$apply so Angular updates the DOM
             $timeout(function() {
                  delete $scope.jobListings[snap.key];
             });
        });

        console.log( `boom-----`);
    }



    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

    // TODO Filter available jobs by search radius, etc
    // $scope.availableJobs = $firebaseArray(FirebaseRef.jobListings().orderByChild('hasEnoughPandas').equalTo(false));
    // $scope.availableJobs = $firebaseArray(JobsService.getOpenJobs());
    // console.log( "BLOOP")


    $scope.user = ProfileService.getUser();
    if( !$scope.user ) {
      $scope.consumerActive = false;
      console.log( '-----------------------' );
      console.log( `Unable to get user for ${user}` );
      console.log( '-----------------------' );
    } else {
      if( $scope.user.$id ) {
        //Allow the jobs list to load for inactive pandas, we filter it out of the GUI later
        // $scope.availableJobs = JobsService.getOpenJobs();
      }
    }

    //This initializes before we know if we're in qa or production so qa does not get the
    //correct job listings
    $scope.jobListings = JobsService.getJobHistory();
  }
})();
