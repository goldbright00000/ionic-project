(function() {
    'use strict';

    angular
    .module('app.consumer')
    .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = [
      '$scope',
      '$state',
      '$stateParams',
      'currentAuth',
      'FirebaseRef',
      'ProfileService',
      'CacheService',
      'JobsService'
    ];

    function HomeCtrl($scope, $state, $stateParams, currentAuth, FirebaseRef, ProfileService, CacheService, JobsService) {
      $scope.newChore = newChore;
      $scope.newFeaturedChore = newFeaturedChore;

      function newFeaturedChore(type) {
        console.log(type);
        CacheService.setChore(type);
        $state.go('app.consumer.create-job.pandasNeeded');
      }

      function newChore() {
        $state.go('app.consumer.create-job.category');
      }

      function myChores() {
        $state.go('app.consumer.jobs.history');
      }
    }
})();
