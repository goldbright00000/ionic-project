(function() {
    'use strict';

    angular
    .module('app.consumer')
    .controller('ConsumerShareCtrl', ConsumerShareCtrl);

    ConsumerShareCtrl.$inject = [
      '$scope',
      '$state',
      '$stateParams',
      'currentAuth',
      'FirebaseRef',
      'ProfileService'
    ];

    function ConsumerShareCtrl($scope, $state, $stateParams, currentAuth, FirebaseRef, ProfileService) {
      console.log('herrr');
      $scope.shareSendText = shareSendText;
      $scope.shareSendEmail = shareSendEmail;
      function shareSendText() {
        $state.go('app.consumer.share.share-text');
      }
      function shareSendEmail() {
       $state.go('app.consumer.share.share-email');
      }
    }
})();
