(function() {

  'use strict';

  angular
    .module('app.consumer')
    .controller('CreateJobCtrl', CreateJobCtrl);

  CreateJobCtrl.$inject = [
    '$scope',
    '$state',
    '$compile',
    'Auth',
    'FirebaseRef',
    'currentAuth',
    '$ionicScrollDelegate',
    '$ionicModal',
    '$ionicHistory',
    '$cordovaDatePicker',
    'JobsService',
    'CacheService',
    'ionicDatePicker',
    'ionicTimePicker',
    '$http',
    '$ionicPopup',
    'StripeCharge',
    'NotificationsService'
  ];

  function CreateJobCtrl($scope, $state, $compile, Auth, FirebaseRef, currentAuth, $ionicScrollDelegate, $ionicModal, $ionicHistory, $cordovaDatePicker, JobsService, CacheService, ionicDatePicker, ionicTimePicker, $http, $ionicPopup, StripeCharge, NotificationsService) {
    const serverURL = CacheService.getServerURL();
    const fetchHourlyRateUrl = serverURL + 'fetchHourlyRate';
    const fetchPandaHourlyRateUrl = serverURL + 'fetchPandaHourlyRate';

    console.log('INIT JOB CREATE CONTROLLER');

    window.Intercom("boot", {
      // app_id: "zk6k6lsv",
      app_id: "rus6c6vh",
      email: currentAuth.email,
      custom_launcher_selector: '#my_custom_link'
    });
    // Local variables
    var newJobId;
    // Scope variables
    $scope.runningTotal = 0;
    $scope.newJob = {
      createdBy: currentAuth.uid,
      createdAt: Date.now(),
      category: '',
      type: '',
      location: {},
      pandasNeeded: 1,
      paymentReceived: false,
      hasEnoughPandas: false,
      pandaCount: 0,
      hourlyRate: 20,
      pandaHourlyRate: 15,
      totalHours: 1,
      hoursUsed: 0,
      timeFlexible: false,
      startTime: Date.now(), //tweak to good default - today, top of the next hour
      endTime: Date.now(),
      notes: '',
      completed: false,
      started: false,
      cancelled: false,
    };



    document.addEventListener("deviceready", function() {
          $scope.showDatePicker = function() {
            console.log('show date picker fired');
            const options = {
              date: $scope.newJob.startTime,
              mode: 'datetime',
              minDate: calculateStartTime(),
              maxDate: moment().add(3, 'months').toDate(),
              allowOldDates: false,
              allowFutureDates: true,
              doneButtonLabel: 'SET',
              doneButtonColor: '#000000',
              cancelButtonLabel: 'CANCEL',
              minuteInterval: 15,
              cancelButtonColor: '#000000'
            };
            $cordovaDatePicker.show(options).then(function(date) {
              if (date) {
                console.log("LOOK HERE, GB", date);
                if (currentState('time')) {
                  console.log("here1");
                  $scope.newJob.startTime = date.getTime();
                  console.log('Changed the start date to ' + $scope.newJob.startTime);
                }
              } else {
                console.log('Date undefined'); // Cancel clicked
              }
            });
          }
        });







    $scope.pandasNeededPicker = {
      inputValue: $scope.newJob.pandasNeeded,
      minValue: 1,
      maxValue: 10,
      format: 'WHOLE',
      titleLabel: 'People Needed',
      setLabel: 'Set',
      closeLabel: 'Close',
      setButtonType: 'button-assertive',
      closeButtonType: 'button-assertive',
      callback: function(val) {
        var valid = $scope.newJob.pandasNeeded ? true : false;
        // set
        if (val) {
          $scope.newJob.pandasNeeded = val;
          calcRunningTotal();
          // closed
        } else {
          if (!valid) {
            $scope.newJob.pandasNeeeded = 1;
            calcRunningTotal();
          }
        }
      }
    };
    $scope.totalHoursPicker = {
      inputValue: 1,
      minValue: 1,
      maxValue: 8,
      precision: 1, //Optional
      decimalStep: 0.5, //Optional
      format: 'DECIMAL',
      titleLabel: 'Total Hours',
      setLabel: 'Set',
      closeLabel: 'Close',
      setButtonType: 'button-positive',
      closeButtonType: 'button-positive',
      callback: function(val) {
        if (val) {
          $scope.newJob.totalHours = val;
          calcRunningTotal();
        }
      }
    };

    $scope.proposedDate = moment().startOf('day');
    $scope.proposedTime = moment().startOf('hour');//.add(1, 'hours');
    $scope.newJob.startTime = calculateStartTime();

    // $scope.$on('$stateChangeSuccess', () => {
    //   const mapElement = document.getElementById('map');
    //   console.log( `State change map == ${mapElement}`);
    //   if( mapElement ) {
    //     initMap();
    //     setMarker();
    //   }
    // });


    $scope.goBack = function() {
      $ionicHistory.goBack();
      // initMap();
      // setMarker();
    };

    $scope.goHome = function() {
      $state.go( 'app.consumer.home' );
    };

    fetchHourlyRate();
    fetchPandaHourlyRate();
    initJobFromCache();
    calcRunningTotal();
    initNewDatePicker();
    initTimePicker();

    $ionicModal.fromTemplateUrl('app/consumer/create-job/congrats-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });
    $scope.$on('modal.hidden', function() {
      $ionicHistory.nextViewOptions({
        historyRoot: true
      });
      $state.go('app.consumer.job-detail', {
        id: newJobId,
        state: NotificationsService.jobDetailState.SHOW_JOB
      });
    });


    $scope.prevType = '';

    // Scope functions
    $scope.setCategory = setCategory;
    $scope.nextView = nextView;
    $scope.determineScroll = determineScroll;
    $scope.flexibleTime = flexibleTime;
    $scope.createJob = createJob;
    $scope.gotoDetailScreen = gotoDetailScreen;
    $scope.showNotes = showNotes;

    // Function implementation
    function setCategory(category) {
      $scope.newJob.category = category;
      $scope.jobTypes = JobsService.getJobTypes(category);
      nextView('type');
    }

    function nextView(state) {
      $state.go('app.consumer.create-job.' + state)
      .then(function() {
        if( state === 'location' ) {
          const mapElement = document.getElementById('map');
          if( mapElement ) {
            initMap();
            setMarker();
          }
        }
      });
    }

    function currentState(state) {
      return $state.current.name === 'app.consumer.create-job.' + state;
    }

    function determineScroll() {
      // If Other clicked, scroll to show Write-In input
      if ($scope.newJob.type === 'Other') {
        $ionicScrollDelegate.resize();
        $ionicScrollDelegate.scrollBottom(true);
      }
      // If Other clicked off, scroll to hide Write-In input
      else if ($scope.prevType === 'Other') {
        $ionicScrollDelegate.scrollBy(0, 45, true);
      }
      // Update prevType to clicked value
      $scope.prevType = $scope.newJob.type;
    }

    function flexibleTime() {
      if ($scope.newJob.timeFlexible) {
        $state.go('app.consumer.create-job.flexible');
      }
    }

    function initNewDatePicker() {
      //jobs can only be scheduled for <90 from now
      let maxDate = moment().add(3, 'months').toDate();

      var ipObj1 = {
        callback: function (val) {  //Mandatory
          // console.log('Return value from the datepicker popup is : ' + val, new Date(val));
          // $scope.newJob.startTime = val;
          $scope.proposedDate = moment(val);
          $scope.proposedDate.startOf('day');
          calculateStartTime();
        },
        from: new Date(),
        to: maxDate,
        mondayFirst: false,
        closeOnSelect: false,
        templateType: 'popup',
        showTodayButton: false
      };

      $scope.openDatePicker = function(){
        ionicDatePicker.openDatePicker(ipObj1);
      };
    }




    function initTimePicker() {
      let hours = new Date().getHours() + 1;
      $scope.openTimePicker = function () {
        var ipObj1 = {
          callback: function (val) {      //Mandatory
            if (typeof (val) === 'undefined') {
              // console.log('Time not selected');
            } else {
              let tmp = moment($scope.proposedDate).startOf('day');
              $scope.proposedTime = tmp.add(val, 'seconds');
              // console.log('Selected epoch is : ', val, 'and the time is ', $scope.proposedTime.format());
              calculateStartTime();
            }
          },
          inputTime: (hours * 60 * 60),   //Optional
          format: 12,         //Optional
          step: 30,           //Optional
          setLabel: 'Set Time'  //Optional


        };
        ionicTimePicker.openTimePicker(ipObj1);
      };
    }

    function createJob(e) {
      e.preventDefault(e);
      // finalizeJobCreation();
      runStripeGUI();
    }

    function runStripeGUI() {
      $scope.stripe_user_id = StripeCharge.getAccountId();
      if( !$scope.stripe_user_id ) {
        showAccountRetrieveError();
        return;
      }

      // $scope.status['loading'] = true;
      // $scope.status['message'] = "Retrieving your Stripe Token...";
      //
      //TODO Add description of work and panda name
      $scope.ProductMeta = {
      //  title: "Thank you! - Chore Team",

        name: 'Thank you',
        description: 'Charged when completed',
        panelLabel: 'Authorize',
        // image: 'img/128px-Block_Anvil.png',
        image: 'https://dl.airtable.com/.attachments/fc2ba60f958ff524bdc5d0b662a0f25d/44553f30/chorestripe.png',
        // priceUSD: 1,
        priceUSD: $scope.newJob.hourlyRate * $scope.newJob.totalHours,
        productUserId: $scope.stripe_user_id, //TODO: pass in product user id
        label: 'help'
      };


    // console.log($scope.ProductMeta, "product meta");
      // first get the Stripe token
      StripeCharge.getStripeToken($scope.ProductMeta).then(
        function(stripeToken) {
          // -->
          console.log("STRIPE TOKEN: ", stripeToken);
          // proceedCharge(stripeToken);
          finalizeJobCreation();
          writeTransactionToDB( stripeToken, $scope.ProductMeta, $scope.newJobId, $scope.newJob );
          $scope.modal.show();
        },
        function(error){
          console.log("STRIPE ERROR", error);

          // $scope.status['loading'] = false;
          // if(error != "ERROR_CANCEL") {
          //   $scope.status['message'] = "Oops... something went wrong";
          // } else {
          //   $scope.status['message'] = "";
          // }
        }
      ); // ./ getStripeToken

    }

    function showAccountRetrieveError() {
      var alertPopup = $ionicPopup.alert({
        title: 'Account Error',
        template: 'There was an error retrieving your account information'
      });
      alertPopup.then(function(res) {
        console.log('Thank you for not eating my delicious ice cream cone');
      });
    };


    /**
     *
     * @param token
     * @param productMeta
     * @param jobId - newly generated job Id. We've already created the record in FB
     * Put the info about the transaction in the DB. Once they final payment is ready, we'll finish the transactions
     */
    function writeTransactionToDB(token, productMeta, jobId, newJob) {
      let pendingData = {
        stripeToken: token,
        productMeta: productMeta,
        jobId: jobId,
        jobType: newJob.type
      };

      FirebaseRef.root.child( '/pendingTransactions/' + jobId).set(pendingData);
    }

    function finalizeJobCreation() {
      console.log('newJob', $scope.newJob.startTime);
      $scope.newJob.location = {
        address_components: $scope.newJob.location.address_components,
        formatted_address: $scope.newJob.location.formatted_address,
        lat: $scope.newJob.location.geometry.location.lat(),
        lng: $scope.newJob.location.geometry.location.lng()
      };

      let t = moment($scope.newJob.startTime);
      // console.log( t.toString())
      // console.log( t.toDate())
      t.add($scope.newJob.totalHours, 'hours');
      $scope.newJob.endTime = t.toDate().valueOf();

      // Push new job to jobs list in db
      var newJobRef = FirebaseRef.jobListings().push($scope.newJob);
      console.log('Created new job with key: ' + newJobRef.key);

      // Add reference to new job to user's jobs list
      FirebaseRef.profiles.child(currentAuth.uid + '/jobListings/' + newJobRef.key).set(true);
      $scope.newJobId = newJobRef.key;

      // TODO Add to calendar modal

      // TODO Social share modal

      newJobId = newJobRef.key;
      // runStripeGUI();
    }

    function gotoDetailScreen(destination) {
      if(Object.keys($scope.newJob.location) !== 0 ) {
        if( $scope.newJob.location.geometry ) {
          nextView(destination);
        }
      } else {
        showMissingAddressError();
      }

    }

    function showNotes() {
      var alertPopup = $ionicPopup.alert({
        cssClass: 'my-custom-popup2',
        okText: 'Close',
        title: 'Notes',


        template: $scope.newJob.notes
      });
      alertPopup.then(function() {});
    };

    function showMissingAddressError() {
      var confirmPopup = $ionicPopup.alert({
        title: 'Address Missing',
        cssClass: 'my-custom-popup',
        template:  "Please provide an address for the job.",
        okText: 'OK',
        okType: 'button-positive',
      });

      confirmPopup.then(function(res) {
        // if(res) {
        //   $scope.goHome();
        // } else {
        // }
      });
    };


    function calcRunningTotal() {
      var total = $scope.newJob.pandasNeeded * $scope.newJob.totalHours * $scope.newJob.hourlyRate;
      $scope.runningTotal = total;
    }

    function initJobFromCache() {
      var presetChore =  CacheService.getChore(); //used for preferred chore set from homepage
      if(presetChore) {
        console.log( `Preferred chore ${CacheService.getChore()}`)
        $scope.newJob.category = 'home'; //set it to something
        $scope.newJob.type = presetChore;

        CacheService.setChore(null);
      }
    }

    function calculateStartTime() {
      // console.log( `Proposed Date ${$scope.proposedDate} Time ${$scope.proposedTime}`);
      // console.log( `Proposed Date ${$scope.proposedTime.hours()} Time ${$scope.proposedTime.minutes()}`);
      let result = moment($scope.proposedDate);
      result.add($scope.proposedTime.hours(), 'h').add($scope.proposedTime.minutes(), 'm');
      console.log( `Result Date ${result.format()}`);
      return result.toDate().valueOf();
    }

    async function fetchHourlyRate() {
      await $http.get(fetchHourlyRateUrl).success(function( res ) {
        // console.log( `Fetched hourly rate: ${res.value}` );
        $scope.newJob.hourlyRate = Number(res.value);
        calcRunningTotal();
      }).error(function(error) {
            console.log( '----------------------------------------' );
            console.log("Unable to retrieve hourly rate from server");
            console.log(error);
            console.log( '----------------------------------------' );
          }
        );
    }

    async function fetchPandaHourlyRate() {
      await $http.get(fetchPandaHourlyRateUrl).success(function( res ) {
        // console.log( `Fetched panda hourly rate: ${res.value}` );
        $scope.newJob.pandaHourlyRate = Number(res.value);
        calcRunningTotal();
      }).error(function(error) {
          console.log( '----------------------------------------' );
          console.log("Unable to retrieve panda hourly rate from server");
          console.log(error);
          console.log( '----------------------------------------' );
        }
      );
    }

    /*
    This code comes from a simple, excellent implementation of basic map functionality at:
    https://github.com/abhaykumar1415/ionicPlacesDemo
     */
    function initMap() {
        var mapOptions = {
          center: { lat: 32.799390, lng: -79.949980 }, //Charleston City Hall
          zoom: 11,
          disableDefaultUI: true, // To remove default UI from the map view
          scrollwheel: false
        };

        $scope.disableTap = function() {
          let container = document.getElementsByClassName('pac-container');
          angular.element(container).attr('data-tap-disabled', 'true');
          let backdrop = document.getElementsByClassName('backdrop');
          angular.element(backdrop).attr('data-tap-disabled', 'true');
          angular.element(container).on("click", function() {
            document.getElementById('pac-input').blur();
          });
        };

        var map = new google.maps.Map(document.getElementById('map'),
          mapOptions);

        const input = (document.getElementById('pac-input'));

        let autocomplete = null;
        if( input ) {
          // Create the autocomplete helper, and associate it with
          // an HTML text input box.
          autocomplete = new google.maps.places.Autocomplete(input);
          autocomplete.setTypes(['address']);
          autocomplete.bindTo('bounds', map);

          map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        }

        $scope.infowindow = new google.maps.InfoWindow();
        $scope.marker = new google.maps.Marker({
          map: map
        });
        google.maps.event.addListener($scope.marker, 'click', function() {
          $scope.infowindow.open(map, $scope.marker);
        });

        //this check is necessary to prevent an error in the transition to the confirm page
        if( !autocomplete ) { return; }

        // Get the full place details when the user selects a place from the
        // list of suggestions.
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
          $scope.infowindow.close();
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            return;
          }

          if( !isPremiseLocation(place) ) {
            const alertPopup = $ionicPopup.alert({
              title: 'Invalid Location',
              template: 'Please specify a more specific location.',
              cssClass: 'my-custom-popup',
            });
            clearMarker();
            alertPopup.then(function (res) {
              document.getElementById('pac-input').value = '';
              map.setCenter({lat: 32.778056, lng: -79.932588});
              map.setZoom(13);
              return;
            });
          } else if(addressOutOfServiceArea(place)) {
            const alertPopup = $ionicPopup.alert({
              title: 'Out of Range',
              template: 'Sorry, that address is out of our current service area.',
              cssClass: 'my-custom-popup',
            });
            clearMarker();
            alertPopup.then(function (res) {
              document.getElementById('pac-input').value = '';
              map.setCenter({lat: 32.778056, lng: -79.932588} );
              map.setZoom(13);
              return;
            });
          } else {
            $scope.newJob.location = place;

            if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
            } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);
            }
            setMarker();
          }
        });

      }

      function setMarker() {
        // Set the position of the marker using the place ID and location.
        let loc = $scope.newJob.location;
        if(Object.keys($scope.newJob.location) == 0 ) return;

        $scope.marker.setPlace(/** @type {!google.maps.Place} */ ({
          placeId: loc.place_id,
          location: loc.geometry.location
        }));
        $scope.marker.setVisible(true);

        let usaIndex = loc.formatted_address.indexOf(', USA');
        var modifiedPlace = loc.formatted_address;
        if (usaIndex > -1) {
          modifiedPlace = modifiedPlace.substr(0, usaIndex);
        }
        $scope.infowindow.setContent('<div><strong>' +
          modifiedPlace + '</strong></div>');
        $scope.infowindow.open(map, $scope.marker);
      }

      function clearMarker() {
        $scope.marker.setVisible(false);
      }

      function addressOutOfServiceArea(place) {
        let result = true;
        place.address_components.forEach( (component) => {
          if(component.types.indexOf('postal_code') > -1) {
            if( component.long_name.startsWith('294')) { result = false; }
          }
        });
        return result;
      }

      function isPremiseLocation(place) {
        if( place.types.find( (type) => {
          if( type === 'premise' || type === 'street_address' ) {
            return true;
          }
        })) {
          return true;
        } else {
          return false;
        }
     }
  }
})();
