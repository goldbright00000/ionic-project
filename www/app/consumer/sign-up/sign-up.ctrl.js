(function() {
  'use strict';

  angular
    .module('app.consumer')
    .controller('ConsumerSignUpCtrl', ConsumerSignUpCtrl);

  ConsumerSignUpCtrl.$inject = [
    '$scope',
    '$state',
    'Auth',
    'FirebaseRef',
    '$cordovaImagePicker',
    '$cordovaFile',
    '$ionicHistory',
    '$ionicPush'
  ];

  function ConsumerSignUpCtrl($scope, $state, Auth, FirebaseRef, $cordovaImagePicker, $cordovaFile, $ionicHistory, $ionicPush) {
    window.Intercom("boot", {
      app_id: "rus6c6vh",
      custom_launcher_selector: '#my_custom_link'
    });
    // VARIABLES
    $scope.user = {
      name: '',
      email: '',
      password: '',
      password_confirm: ''
    };

    resetErrors();

    $scope.register = function(form) {

      if (form.$valid) {
        resetErrors();
        console.log('Creating user...');

        // Create a new user
        Auth.register($scope.user)
          .then(function(result) {
            console.log( result );

            localStorage.setItem("ppuid", result.user.uid);
            localStorage.setItem("role", result.role);

            // User successfully created
            console.log('Created user: ' + result.user.uid + ', adding user profile to db...');

            // Add user profile to db
            FirebaseRef.profiles.child(result.user.uid).set({
              firstName: $scope.user.firstName,
              lastName: $scope.user.lastName,
              email: $scope.user.email,
              mobilePhone: $scope.user.mobilePhone,
              role: 'consumer',
              profilePhoto: null,
              ratings:{'000000': {'rating':'5'}}
            });

            // Added profile to db, redirect to assessment
            console.log('Added profile to db, routing to add profile picture...');
            $ionicHistory.nextViewOptions({
              historyRoot: true
            });
            $state.go('app.consumer.onboarding-1');
          }).catch(function(error) {

            // On Auth error, set error message
            setError(error.code);
            console.log('Error creating user: ' + error.code + ' - ' + error.message);
          });
      } else {
        console.log('Form invalid');
        formTouched(form);
      }
    };


    $scope.onboarding2 = function() {
      $state.go('app.consumer.onboarding-2');
    };

    $scope.onboarding3 = function() {
      $state.go('app.consumer.onboarding-3');
    };

    $scope.onboardingExit = function() {
      $state.go('app.consumer.profile-picture');
    };

    // Clear error message in DOM
    function resetErrors() {
      $scope.error = {
        email: '',
        phone: '',
        password: '',
        message: ''
      };
    }

    // Set all inputs to $touched to display error messages
    function formTouched(form) {
      form.firstName.$touched = true;
      form.lastName.$touched = true;
      form.email.$touched = true;
      form.password.$touched = true;
      form.confirmPassword.$touched = true;
      form.confirmPassword.$dirty = true;
      form.mobilePhone.$touched = true;
    }

    // Set error message in DOM depending on Firebase error code
    function setError(errorCode) {
      switch (errorCode) {
        case 'auth/invalid-email':
          $scope.error.email = true;
          $scope.error.message = 'Invalid email';
          break;
        case 'auth/email-already-in-use':
          $scope.error.email = true;
          $scope.error.message = 'Email already in use';
          break;
        case 'auth/weak-password':
          $scope.error.password = true;
          $scope.error.message = 'Password not strong enough';
          break;
        default:
          $scope.error.message = 'There was an error signing you up.';
      }
    }

    $scope.editProfile = function() {
      // console.log(profiles.firstName);
      $state.go('app.consumer.edit-profile');
    }

    $scope.updateProfile = function() {
      console.log( 'FOOP' );
      scope.profile.$save()
      console.log($scope.profile.firstName);
    }
  }
})();
