(function() {
  'use strict';

  angular
    .module('app.consumer')
    .controller('ConsumerProfilePictureCtrl', ConsumerProfilePictureCtrl);

  ConsumerProfilePictureCtrl.$inject = [
    '$scope',
    '$state',
    'Auth',
    'currentAuth',
    'FirebaseRef',
    '$cordovaImagePicker',
    '$cordovaFile',
    '$ionicPlatform',
    '$ionicHistory',
    '$q',
    '$timeout'
  ];

  function ConsumerProfilePictureCtrl($scope, $state, Auth, currentAuth, FirebaseRef, $cordovaImagePicker, $cordovaFile, $ionicPlatform, $ionicHistory, $q, $timeout) {
    var fileName;
    var path;

    //NB: This is only called on the device. You will not get this signal in the browser.
    document.addEventListener("deviceready", function() {
      console.log("cord f1", cordova[0]);
      console.log("cord f2", $cordovaFile[0]);
      function modifyImage(results) {

        fileName = results[0].replace(/^.*[\\\/]/, '');

        // modify the image path when on Android
        if ($ionicPlatform.is('android')) {
          path = cordova.file.cacheDirectory;
        } else  {
          path = cordova.file.tempDirectory;
          console.log('path ', path);
        }

        return $cordovaFile.readAsDataURL(path, fileName);
      }

      function saveToFirebase(file) {

        var userProfileRef = FirebaseRef.profiles.child(currentAuth.uid);

        return userProfileRef.child('profilePicture').set(file);
      }

      // Got the core of this functionality from aaronksaunders
      // https://github.com/aaronksaunders/firebaseStorageApp
      // $scope.doGetImage = function() {
        // var pickerArgs = {
        //     'selectMode': 100, //101=picker image and video , 100=image , 102=video
        //     'maxSelectCount': 1, //default 40 (Optional)
        //     'maxSelectSize': 188743680, //188743680=180M (Optional) only android
        // };
        //
        // MediaPicker.getMedias(pickerArgs, function(medias) {
        //     //medias [{mediaType: "image", path:'/storage/emulated/0/DCIM/Camera/2017.jpg', uri:"android retrun uri,ios retrun URL" size: 21993}]
        //     getThumbnail(medias);
        //
        //   }, function(e) { console.log('Error in getMedias');
        // });
        //
    //     function getThumbnail(medias) {
    //       for (var i = 0; i < medias.length; i++) {
    //           medias[i].thumbnailQuality=50;
    //           //loadingUI(); //show loading ui
    //           MediaPicker.extractThumbnail(medias[i], function(data) {
    //               // saveToFirebase(data);
    //               console.log('=======================');
    //               Object.keys(data).forEach(key => console.log(key));
    //               console.log('=======================');
    //
    //               const imageData = 'data:image/jpeg;base64,' + data.thumbnailBase64;
    //               console.log(`|||||------- ${document.getElementsByName('imgView')[0]} ------------||||`);
    //               document.getElementsByName('imgView')[0].src = imageData;
    //               document.getElementsByName('imgView')[0].setAttribute('style', 'transform:rotate(' + data.exifRotate + 'deg)');
    //               saveToFirebase(imageData);
    //           }, function(e) { console.log(e) });
    //       }
    //     }
    //
    //   };
    // }, false);
  // }

    $ionicHistory.nextViewOptions({
      disableBack: true
    });

    $scope.goJobsList = function() {
      $state.go('app.panda.jobs-list');
    };

    $scope.goHome = function() {
      $state.go('app.consumer.home');
    };
  });
}
})();
