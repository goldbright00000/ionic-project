(function() {
    'use strict';

    angular
    .module('app.consumer')
    .config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {
        $stateProvider
            .state('app.consumer', {
                url: '/consumer',
                abstract: true,
                views: {
                  'menuContent': {
                    controller: function() {
                      console.log('In consumer module controller');
                    }
                  }
                }
            })
            .state('app.consumer.home', {
                url: '/home',
                views: {
                    'menuContent@app': {
                        controller: 'HomeCtrl',
                        templateUrl: 'app/consumer/home/home.html'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.consumer.share', {
                url: '/share',
                views: {
                    'menuContent@app': {
                        controller: 'ConsumerShareCtrl',
                        templateUrl: 'app/consumer/share/share.html'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.consumer.share.share-text', {
                url: '',
                views: {
                    'menuContent@app': {
                          templateUrl: 'app/consumer/share/share-text.html'
                    }
                }
            })
            .state('app.consumer.share.share-email', {
                url: '',
                views: {
                    'menuContent@app': {
                          templateUrl: 'app/consumer/share/share-email.html'
                    }
                }
            })
            .state('app.consumer.sign-up', {
                url: '/sign-up',
                views: {
                    'menuContent@app': {
                        controller: 'ConsumerSignUpCtrl',
                        templateUrl: 'app/consumer/sign-up/sign-up.html'
                    }
                }
            })
            .state('app.consumer.onboarding-1', {
                url: '/onboarding-1',
                views: {
                    'menuContent@app': {
                        controller: 'ConsumerSignUpCtrl',
                        templateUrl: 'app/consumer/sign-up/onboarding-1.html'
                    }
                }
            })
            .state('app.consumer.onboarding-2', {
                url: '/onboarding-2',
                views: {
                    'menuContent@app': {
                        controller: 'ConsumerSignUpCtrl',
                        templateUrl: 'app/consumer/sign-up/onboarding-2.html'
                    }
                }
            })
            .state('app.consumer.onboarding-3', {
                url: '/onboarding-3',
                views: {
                    'menuContent@app': {
                        controller: 'ConsumerSignUpCtrl',
                        templateUrl: 'app/consumer/sign-up/onboarding-3.html'
                    }
                }
            })
            .state('app.consumer.profile-picture', {
                url: '/profile-picture',
                views: {
                    'menuContent@app': {
                        controller: 'ConsumerProfilePictureCtrl',
                        templateUrl: 'app/consumer/sign-up/profile-picture.html'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.consumer.settings', {
                url: '/settings',
                views: {
                    'menuContent@app': {lateUrl: 'app/consumer/settings/settings.html',
                        controller: 'ConsumerSettingsCtrl'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.consumer.create-job', {
              cache: false, //need controller to initialize every time since sometimes we skip to notes page
                url: '/create-job',
                abstract: true,
                views: {
                  'menuContent@app': {
                    controller: 'CreateJobCtrl',
                    template: '<ion-nav-view name="createJob"></ion-nav-view>'
                  }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.consumer.create-job.category', {
                url: '',
                views: {
                    'createJob': {
                          templateUrl: 'app/consumer/create-job/job-category.html'
                    }
                }
            })
            .state('app.consumer.create-job.type', {
                url: '/type',
                views: {
                    'createJob': {
                          templateUrl: 'app/consumer/create-job/job-type.html'
                    }
                }
            })
            .state('app.consumer.create-job.notes', {
                url: '/notes',
                views: {
                    'createJob': {
                          templateUrl: 'app/consumer/create-job/job-notes.html'
                    }
                }
            })
            .state('app.consumer.create-job.pandasNeeded', {
                url: '/pandas-needed',
                views: {
                    'createJob': {
                          templateUrl: 'app/consumer/create-job/job-pandasNeeded.html'
                    }
                }
            })
            .state('app.consumer.create-job.totalHours', {
                url: '/total-hours',
                views: {
                    'createJob': {
                          templateUrl: 'app/consumer/create-job/job-totalHours.html'
                    }
                }
            })
            .state('app.consumer.create-job.location', {
                url: '/location',
                views: {
                    'createJob': {
                        templateUrl: 'app/consumer/create-job/job-location.html'
                    }
                }
            })
            .state('app.consumer.create-job.time', {
                url: '/time',
                views: {
                    'createJob': {
                        templateUrl: 'app/consumer/create-job/job-time.html'
                    }
                }
            })
            .state('app.consumer.create-job.flexible', {
                url: '/flexible',
                views: {
                    'createJob': {
                        templateUrl: 'app/consumer/create-job/job-flexible.html'
                    }
                }
            })
            .state('app.consumer.stripe', {
                url: '/stripe',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/panda/sign-up/stripe.html',
                        controller: 'StripeController'
                    }
                }
            })
            .state('app.consumer.create-job.confirm', {
                url: '/confirm',
                views: {
                    'createJob': {
                        templateUrl: 'app/consumer/create-job/job-confirm.html'
                    }
                }
            })
            .state('app.consumer.test', {
                views: {
                    'createJob': {
                        controller: 'TestCtrl',
                        templateUrl: 'app/consumer/test/test.html'
                    }
                }
            })
            .state('app.consumer.job-detail', {
                url: '/job/:id/:state',
                views: {
                  'menuContent@app': {
                    controller: 'ConsumerJobDetailCtrl as ctrl',
                    templateUrl: 'app/consumer/job-detail/job-detail.html'
                  }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })


            .state('app.consumer.jobs-history', {
                url: '/jobs-history',
                views: {
                    'menuContent@app': {
                        templateUrl: 'app/consumer/jobs-history/jobs-history.html',
                        controller: 'ConsumerJobsHistoryCtrl'
                    }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.consumer.chat-list', {
                url: '/chats/:id',
                views: {
                  'menuContent@app': {
                    controller: 'ConsumerChatListCtrl',
                    templateUrl: 'app/consumer/chat/chat-list.html'
                  }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            })
            .state('app.consumer.chat-room', {
                url: '/chat/:jobId/:roomId',
                views: {
                  'menuContent@app': {
                    controller: 'ConsumerChatDetailCtrl',
                    templateUrl: 'app/consumer/chat/chat-room.html'
                  }
                },
                resolve: {
                   'currentAuth': ['Auth', function(Auth) {
                       return Auth.requireSignIn();
                   }]
               }
            });
    }
})();
