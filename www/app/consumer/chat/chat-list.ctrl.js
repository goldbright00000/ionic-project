(function() {
    'use strict';

    angular
    .module('app.consumer')
    .controller('ConsumerChatListCtrl', ConsumerChatListCtrl);

    ConsumerChatListCtrl.$inject = [
      '$scope',
      '$state',
      '$stateParams',
      '$ionicModal',
      'currentAuth',
      'FirebaseRef',
      'JobsService',
      'ProfileService'
    ];

    function ConsumerChatListCtrl($scope, $state, $stateParams, $ionicModal, currentAuth, FirebaseRef, JobsService, ProfileService) {

        $scope.job = JobsService.getJob($stateParams.id);
        $scope.chatRooms = [];

        var chatRoom;
        // Get list of job chat rooms
        FirebaseRef.jobChatRooms.child($stateParams.id).on('child_added', function(roomSnap) {
            FirebaseRef.profiles.child(roomSnap.val().pandaId).once('value', function(profileSnap) {
                chatRoom = roomSnap.val();
                chatRoom.id = roomSnap.key;
                chatRoom.Chore= profileSnap.val();

                $scope.chatRooms.push(chatRoom);
            });
        });
    }
})();
