(function() {
    'use strict';

    angular
    .module('app.consumer')
    .controller('ConsumerChatDetailCtrl', ConsumerChatDetailCtrl);

    ConsumerChatDetailCtrl.$inject = [
      '$scope',
      '$state',
      '$stateParams',
      '$ionicScrollDelegate',
      'currentAuth',
      'FirebaseRef',
      'ChatService',
      'ProfileService',
      '$ionicHistory'
    ];

    function ConsumerChatDetailCtrl($scope, $state, $stateParams, $ionicScrollDelegate, currentAuth, FirebaseRef, ChatService, ProfileService, $ionicHistory) {
        ChatService.getChatRoom($stateParams.jobId, $stateParams.roomId).$loaded().then(function(res) {
            $scope.room = res;
            $scope.panda = ProfileService.fetchProfile(res.pandaId);
        });

        $scope.goBack = function() {
          $ionicHistory.goBack();
        }

        $scope.messages = ChatService.getChatMessages($stateParams.roomId);
        // console.log( "=======")
        // console.log( $scope.messages );
        window.msg = $scope.messages;

        // Add advisor message to messages array
        $scope.submitMessage = function() {
            var now = Date.now();
            var newMessage = {
                author: currentAuth.uid,
                content: $scope.newMessageContent,
                createdAt: now
            };

            $scope.messages.$add(newMessage).then(function(ref) {
              var id = ref.key;
              console.log("Added message with id " + id);
              $scope.newMessageContent = '';

              $scope.room.lastMessage = newMessage;
              $scope.room.$save().then(function(ref) {
                  console.log('Saved message as last room message');
              }, function(error) {
                  console.log("Error:", error);
              });
            });
        }

        $scope.$watch('messages', function(newValue, oldValue) {
            // $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollBottom(false);
        }, true);
    }
})();
