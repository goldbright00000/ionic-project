
(function() {
    'use strict';

    angular
    .module('app.consumer')
    .controller('ConsumerJobDetailCtrl', ConsumerJobDetailCtrl);

    ConsumerJobDetailCtrl.$inject = [
      '$scope',
      '$state',
      '$stateParams',
      '$ionicModal',
      'currentAuth',
      'FirebaseRef',
      'JobsService',
      'ProfileService',
      'ModalService',
      '$ionicNavBarDelegate',
      'StripeCharge',
      'StripeCheckout',
      '$ionicPopup',
      '$ionicHistory',
      '$firebaseObject',
      'NotificationsService',
      'CacheService',
      '$http',
      'stripeKey'
    ];

    function ConsumerJobDetailCtrl($scope, $state, $stateParams, $ionicModal, currentAuth, FirebaseRef, JobsService, ProfileService, ModalService, $ionicNavBarDelegate, StripeCharge, StripeCheckout, $ionicPopup, $ionicHistory, $firebaseObject, NotificationsService, CacheService, $http, stripeKey) {

        const serverURL = CacheService.getServerURL();
        const startJobURL = serverURL + 'job/startTimer';
        const addRatingURL = serverURL + 'addRatingForUser';
        const DEFAULT_RATING = 5;

        $ionicModal.fromTemplateUrl('app/consumer/job-detail/confirm-charge.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.chargeModal = modal;
        });

        var handler = StripeCheckout.configure({
            name: "Payment",
            key: stripeKey.key,
            token: function(token, args) {
              $log.debug("Got stripe token: " + token.id);
            }
        });


        // key: "pk_test_KB2qT687K8gtJw7JuF4k3Mqm",
        StripeCheckout.defaults({
          opened: function() {
            console.log('Stripe Checkout opened');
          },

          closed: function() {
            console.log('Stripe Checkout closed');
          }
        });

        this.doCheckout = function(token, args) {
          var options = {
            //TODO change to worker name later
            description: 'Payment for completed work',
            amount: $scope.costInPennies
          };

          handler.open(options)
            .then(function(result) {
              alert('Got Stripe token: ' + result[0].id);
            },function() {
              alert('Stripe Checkout closed without making a sale :(');
          });
        };

        $scope.allApplicants = [];
        $scope.applications = [];
        $scope.pendingRating = DEFAULT_RATING;
        $scope.userRatingSet = false;

      console.log(`LOADING JOB JOB ${$stateParams.id} ${$stateParams.state}`);

      $scope.job = JobsService.getJob($stateParams.id).$loaded(function(res) {
            $scope.job = res;
            $scope.cost = $scope.job.totalHours * $scope.job.hourlyRate * $scope.job.pandasNeeded;
            // $scope.costInPennies = $scope.cost * 100;
            $scope.costInPennies = 100;
            setPandaImage();

        if( $stateParams.state === NotificationsService.jobDetailState.REQUEST_TO_START ||
            jobStartRequestPending() ) {
              console.log( 'REQUESTING to START JOB' );
              showRequestToStart();
            } else if( $stateParams.state === NotificationsService.jobDetailState.RATE_USER ) {
              $scope.showRatingModal();
              console.log( 'SHOW THE RATING SCREEN');
            }
        });

      $scope.showAppDetail = function(x) {
          $scope.applicationDetail = x;
          console.log($scope.applicationDetail);

          ModalService
            .applicationDetail('app/consumer/job-detail/job-application-modal.html', $scope)
            .then(function(modal) {
              modal.show();
            });
      };

      $scope.call = function() {
        if ($scope.job.pandasAccepted) {
          const pandaId = Object.keys($scope.job.pandasAccepted)[0];
          FirebaseRef.profiles.child(pandaId).on('value', function(snapshot) {
            if (snapshot.val() && $scope.consumer) {
              // $http.get('https://us-central1-purple-panda-ad119.cloudfunctions.net/call?to=1'+snapshot.val().mobilePhone+'&from=1' + $scope.consumer.mobilePhone).then(res => {
              //   if (res.status === 200) {
              //     $ionicPopup.alert({
              //       cssClass: 'my-custom-popup',
              //       title: 'Call Status',
              //       template: 'Call succeeded!',
              //       cssClass: 'my-custom-popup',
              //     });
              //   } else {
              //     $ionicPopup.alert({
              //       cssClass: 'my-custom-popup',
              //       title: 'Call Status',
              //       template: 'Call failed!',
              //       cssClass: 'my-custom-popup',
              //     })
              //   }
              // });
            }
          });
        }
      }

    $scope.showRatingModal = () => {
        $scope.applicationDetail = $scope;

        ModalService
          .applicationDetail('app/core/rate-user-modal.html', $scope)
          .then(function(modal) {
            $scope.currentModal = modal;
            modal.show();
          });
      };

      $scope.onRatingsClick = (event) => {
        $scope.userRatingSet = true;
        $scope.pendingRating = event.rating;
      };

      $scope.closeRatingModal = () => {
        if( $scope.currentModal ) {
          ModalService.userPressedOK($scope.currentModal);
          sendNewRatingToServer();
        }
      };

      $scope.$on('$stateChangeSuccess', function()
      {
        setPandaImage();
      });

      $scope.goHome = function() {
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.consumer.home');
      };

       $scope.showConfirmCancel = function() {
         var confirmPopup = $ionicPopup.confirm({
           cssClass: 'my-custom-popup',
           title: 'Cancel Job',
           template: 'Are you sure you want to cancel this chore?',
           cancelText: 'No',
           okText: 'Yes',
           cancelType: 'button-assertive',
           okType: 'button-assertive',
          // okType: 'button-positive',

         });

         confirmPopup.then(function(res) {
           if(res) {
             $scope.cancelJob();
             $scope.goHome();
           } else {
           }
         });
       };

         $scope.showError = function(errorMessage) {
           var errorPopup = $ionicPopup.alert({
             title: errorMessage,
             okText: 'OK'
           });

           errorPopup.then(function(res) {
           });
         };

        $scope.move = function() {
            console.log("Starting job timer");
            var elem = document.getElementById("myBar");
            var width = 0;
            var id = setInterval(frame, 60000);
            function frame() {
                if (width >= 100) {
                  clearInterval(id);
                } else {
                  width++;
                  elem.style.width = width + '%';
                  elem.innerHTML = width * 1 + 'min';
                }
            }
        };

        function setPandaImage() {
          if( !$scope.job || !$scope.job.pandasAccepted ) {
            return;
          }

          //TODO Handle multiple pandas properly
          let panda1 = Object.keys($scope.job.pandasAccepted)[0];

          ProfileService.fetchProfile(panda1).$loaded(function(profile) {
            console.log( `PROFILE =========|||||||| ${panda1}`);
            $scope.pandaProfile = $scope.userProfile = profile;
          });
        }

        $scope.isPast = function(job) {
          const endMoment = moment(job.endTime);
          return moment().isAfter(endMoment);
        }

        $scope.cancelJob = function() {
          $scope.job.cancelled = true;
          $scope.job.completed = true;
          $scope.job.$save()
              .then(function(url) {
                  console.log('Job ' + $scope.job.$id + ' deleted from jobs db.');
                  if( $scope.job.pandasAccepted ) {
                    var keys = Object.keys($scope.job.pandasAccepted);
                    keys.forEach( panda => {
                      var pRef = FirebaseRef.profiles.child(panda + '/jobListings/' + $scope.job.$id);
                      pRef.remove()
                          .then( () => {
                            NotificationsService.sendNotificationToPanda($scope.job, 'Chore Canceled', 'One of the jobs you signed up for has been canceled.',
                              NotificationsService.jobDetailState.SHOW_JOB);

                            console.log(`Job ${panda} ref removed from user profile.`);
                          })
                          .catch(function(error) {
                              console.log("Chore remove failed: " + error.message);
                          });
                    });
                  }

                let ref = FirebaseRef.profiles.child(currentAuth.uid + '/jobListings/' + $scope.job.$id);
                  ref.remove()
                      .then(function() {
                          console.log(`Job ${$scope.job.$id} ref removed from user profile.`);
                      })
                      .catch(function(error) {
                          console.log("Remove failed: " + error.message);
                      });
              })
              .catch( function(error) {
                  console.log("Error:", error);
              });
        };

        $scope.jobCanBeCancelled = function() {
          return $scope.job && !$scope.job.started;
        };

        $scope.jobCanBePayed = function() {
          return $scope.job && !$scope.job.paymentReceived;
        };

        $scope.isBooked =function() {
          return $scope.job.pandasNeeded === $scope.acceptedWorkersLength();
        };

        // $scope.goToChat = function() {
        //     FirebaseRef.jobChatRooms.child($scope.job.$id).on('value', function(snapshot) {
        //         var chatRoomsObj = snapshot.val();
        //         var chatRooms = Object.keys(chatRoomsObj);
        //
        //         if(chatRooms.length == 1) {
        //             $state.go('app.consumer.chat-room', { jobId: $scope.job.$id, roomId: chatRooms[0] });
        //         }
        //         else {
        //             $state.go('app.consumer.chat-list', { id: $scope.job.$id });
        //         }
        //     });
        // };




        $scope.goToChat = function() {
            FirebaseRef.jobChatRooms.child($scope.job.$id).on('value', function(snapshot) {
                var chatRoomsObj = snapshot.val();
                var chatRooms = Object.keys(chatRoomsObj);

                if(chatRooms.length == 1) {
                  $state.go('app.consumer.chat-room', { jobId: $scope.job.$id, roomId: chatRooms[0]   });
                }
                else {
                  $state.go('app.consumer.chat-room', { jobId: $scope.job.$id, roomId: chatRooms[0]
                    });
                }
            });
        };



        $scope.acceptedWorkersLength = function() {
          if( !$scope.job.pandasAccepted ) return 0;

          return Object.keys($scope.job.pandasAccepted).length;
        };

        $scope.status = {
          loading: false,
          message: "",
        };

      $scope.showNotes = function() {
        var alertPopup = $ionicPopup.alert({
          title: 'Notes',
          cssClass: 'my-custom-popup2',
          okText: 'Close',
          template: $scope.job.notes
        });
        alertPopup.then(function() {});

      };

      $scope.finalizePayment = function (e) {
        e.preventDefault(e);

        $scope.status['message'] = "Finalizing payment...";
        FirebaseRef.pendingTransactions.child($scope.job.$id).once('value').then(function(snapshot) {
          const obj = snapshot.val();
          StripeCharge.chargeUser(obj.stripeToken, obj.productMeta, obj.jobId).then (
            function(StripeInvoiceData){
              $scope.status['loading'] = false;
              $scope.status['message'] = "Success! Check your Stripe Account";
              $scope.chargeTitle = 'Thank you!';
              $scope.chargeMessage = `Your card has been charged $${obj.productMeta.priceUSD}`;
              $scope.chargeModal.show().then(function() {
                $scope.goHome();
              }).catch(function(){
                console.log('no modal');
              });
              let id = snapshot.jobId;
              FirebaseRef.jobListings().child(obj.jobId + '/payment/' + StripeInvoiceData.id).set(StripeInvoiceData);
              FirebaseRef.jobListings().child(obj.jobId + '/paymentReceived').set(true);
              FirebaseRef.pendingTransactions.child(obj.jobId).remove();

              console.log(StripeInvoiceData);
            },
            function(error){
              console.log(error);

              $scope.status['loading'] = false;
              $scope.status['message'] = "Oops... something went wrong";
              $scope.chargeTitle = 'Card Processing Problem';
              $scope.chargeMessage = `Your card has not been charged`;
              $scope.chargeModal.show().then(function() {
                console.log('modal');
              }).catch(function(){
                console.log('no modal');
              });
            }
          );
        });
      }; // ./ proceedCharge

      function sendNewRatingToServer() {
        $http.post(addRatingURL, { jobId:$scope.job.$id,
                                    userId:$scope.pandaProfile.$id,
                                    rating:$scope.pendingRating} )
          .success(function() {
            console.log( `New rating added to user ${$scope.pandaProfile.firstName} ${$scope.pandaProfile.lastName} for job ${$scope.job.$id}` );
          })
          .error(
            function(error) {
              console.log(`Error starting job - ${error}`);
            });

      }

      function jobStartRequestPending() {
        return $scope.job.requestToStart && !$scope.job.started;
      }

      function showRequestToStart() {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Start Request',
          cssClass: 'my-custom-popup',
          template: 'Your chore hero has requested to start working.',
          okText: 'OK',
          okType: 'button-positive',
          cancelText: 'Not yet',
          cancelType: 'button-positive' +
            ''
        });

        confirmPopup.then(function(res) {
          if(res) {
            // $scope.job.started = true;
            // $scope.job.$save();
            $http.post(startJobURL, { jobId:$scope.job.$id } )
            .success(function() {
              NotificationsService.sendNotificationToPanda($scope.job, 'Chore', 'The customer has approved you to start working.',
                NotificationsService.jobDetailState.APPROVE_REQUEST_TO_START);
            })
            .error(
              function(error) {
                console.log(`Error starting job - ${error}`);
              });
          } else {
            console.log('Request to start denied');
          }
        });

      }
}
})();
