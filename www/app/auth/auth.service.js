(function() {
    'use strict';

    angular
        .module('app.auth')
        .factory('Auth', Auth);

    //NB: $firebaseAuth comes from angular fire
    Auth.$inject = ['$firebaseAuth', 'FirebaseRef'];

    function Auth($firebaseAuth, FirebaseRef) {
        var firebaseAuthObject = $firebaseAuth();

        var service = {
            firebaseAuthObject: firebaseAuthObject,
            register: register,
            login: login,
            logout: logout,
            requireSignIn: requireSignIn
        };

        return service;

        ////////////

        function register(user) {
            return firebaseAuthObject.$createUserWithEmailAndPassword(user.email, user.password);
        }

        function login(user) {
            return firebaseAuthObject.$signInWithEmailAndPassword(user.email, user.password);
        }

        function logout() {
            return firebaseAuthObject.$signOut();
        }

        function requireSignIn() {
            return firebaseAuthObject.$requireSignIn();
        }

    }
})();
