(function() {
    'use strict';

    angular
    .module('purplePanda')
    .controller('NoLoginPasswordResetController', NoLoginPasswordResetController);

    NoLoginPasswordResetController.$inject = [
      '$scope',
      '$state',
      'Auth',
      'FirebaseRef',
      '$firebaseObject',
      'ModalService',
      '$ionicHistory',
      '$ionicPush'
    ];

    function NoLoginPasswordResetController($scope, $state, Auth, FirebaseRef, $firebaseObject, ModalService, $ionicHistory, $ionicPush) {
        // VARIABLES

        $scope.user = {
            email: '',
        };
        resetErrors();

        // Submit email and password to Auth, redirect on success
        $scope.resetPassword = function(form) {
            resetErrors();
            console.log('Resetting password for user ' + $scope.user.email + '...');

            if (form.$valid) {
              var auth = firebase.auth();
              var emailAddress = $scope.user.email;

              auth.sendPasswordResetEmail(emailAddress).then(function() {
                console.log( "Password reset email sent.")
                ModalService
                        .applicationDetail('app/sign-in/resetMessageModal.html', $scope)
                        .then(function(modal) {
                          modal.show();
                        });
                $ionicHistory.nextViewOptions({
                    historyRoot: true
                });

                //Go back to the start page
                $state.go('app.landing');
              }).catch(function(error) {
                console.log( "Reset email failed to send.");
                console.log( error );
              });
            } else {
                console.log("Form invalid");

            }
        };

        // Clear error message in DOM
        function resetErrors() {
            $scope.error = {
                email: '',
            };
        }

        // Set error message in DOM depending on Firebase error code
        function setError(errorCode) {
            switch (errorCode) {
                case 'auth/invalid-email':
                    $scope.error.email = true;
                    $scope.error.message = 'Invalid email';
                    break;
                default:
                    $scope.error.message = 'There was an error signing you up.';
            }
        }
    }
})();
