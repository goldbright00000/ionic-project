(function() {
    'use strict';

    angular
    .module('purplePanda')
    .controller('SignInController', SignInController);

    SignInController.$inject = [
      '$scope',
      '$state',
      'Auth',
      'FirebaseRef',
      '$firebaseObject',
      '$ionicHistory',
      '$rootScope',
      'ProfileService',
      'CacheService'
    ];

    function SignInController($scope, $state, Auth, FirebaseRef, $firebaseObject, $ionicHistory, $rootScope, ProfileService, CacheService) {
        // VARIABLES

        $scope.user = {
            email: '',
            password: ''
        };
        resetErrors();

        // Submit email and password to Auth, redirect on success
        $scope.signIn = function(form) {
            resetErrors();
            console.log('Logging in users ' + $scope.user.email + '...');

            if (form.$valid) {

                Auth.login($scope.user)
                .then(function (authObj) {

                    // User logged in, get user object from db
                    console.log('Logged in users: ' + authObj.user.uid);
                    $rootScope.isLoggedIn = true;
                    localStorage.setItem('ppuid', authObj.user.uid);
                    localStorage.setItem('role', authObj.user.role);
                    firebase.database().goOnline();
                    var profileRef = FirebaseRef.profiles.child(authObj.user.uid);
                    $firebaseObject(profileRef).$loaded( function(profile) {
                        // console.log(profile);
                        $ionicHistory.nextViewOptions({
                            historyRoot: true
                        });

                        //This is so that browser testing doesn't sent notifcations to other devices
                        FirebaseRef.deviceTokens.child(profile.$id).remove();

                        fetchAndSaveFCMToken(authObj.user.uid);

                        ProfileService.setUser(profile);
                        localStorage.setItem("role", profile.role);
                        if (profile.role == 'consumer') {
                            $state.go('app.consumer.home');
                        } else {
                            $state.go('app.panda.jobs-list');
                        }
                    }, function(error) {
                        console.log('Error getting users: ' + error);
                    });
                }).catch(function(error) {
                    // On Auth error, hide loading overlay and set error message
                    console.log('Error logging in: ' + error.code + ' - ' + error.message);
                    setError(error.code);
                    $('.incorrectPassword').removeClass('hidden');
                });
            } else {
                console.log("Form invalid");

            }
        };

        //TODO this is duplicate code from landing - move toa service
        function fetchAndSaveFCMToken(uid) {
          try {
            FCMPlugin.getToken ( //TODO consider saving token to local storage?
              function (token) {
                console.log('Token: ' + token);
                CacheService.setToken(token);
                FirebaseRef.deviceTokens.child(uid).set(token);
              },
              function (err) {
                // alert('failed to get token: ' + token);
                console.log('error retrieving token: ' + err);
              });
          } catch(err) {
            console.log( `Error fetching token ${err}` );
          }
        }

        // Clear error message in DOM
        function resetErrors() {
            $scope.error = {
                email: '',
                phone: '',
                password: '',
                message: ''
            };
        }

        // Set error message in DOM depending on Firebase error code
        function setError(errorCode) {
            switch (errorCode) {
                case 'auth/invalid-email':
                    $scope.error.email = true;
                    $scope.error.message = 'Invalid email';
                    break;
                case 'auth/email-already-in-use':
                    $scope.error.email = true;
                    $scope.error.message = 'Email already in use';
                    break;
                case 'auth/weak-password':
                    $scope.error.password = true;
                    $scope.error.message = 'Password not strong enough';
                    break;
                default:
                    $scope.error.message = 'There was an error signing you up.';
            }
        }
    }
})();
