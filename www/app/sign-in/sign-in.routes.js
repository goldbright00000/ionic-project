(function() {
    'use strict';

    angular
      .module('app.sign-in')
      .config(configFunction);

    configFunction.$inject = ['$stateProvider'];

    function configFunction($stateProvider) {
        $stateProvider
            .state('app.sign-in', {
                url: '/sign-in',
                views: {
                    'menuContent': {
                        templateUrl: 'app/sign-in/sign-in.html',
                        controller: 'SignInController'
                    }
                }
            })
          .state('app.password-reset', {
            url: '/password-reset',
            views: {
              'menuContent': {
                templateUrl: 'app/sign-in/nologin-reset.html',
                controller: 'NoLoginPasswordResetController'
              }
            }
          })
        ;
    }
})();
